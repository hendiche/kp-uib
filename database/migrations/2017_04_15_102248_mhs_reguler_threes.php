<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MhsRegulerThrees extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mhs_reguler_tigas', function(Blueprint $table) {
          $table->increments('id');
          $table->string('thn_akademik');
          $table->integer('daya_tampung');
          $table->integer('calon_mhs_ikut');
          $table->integer('calon_mhs_lulus');
          $table->integer('mhs_baru_no_trans');
          $table->integer('mhs_baru_trans');
          $table->integer('total_mhs_no_trans');
          $table->integer('total_mhs_trans');
          $table->integer('lulusan_no_trans');
          $table->integer('lulusan_trans');
          $table->integer('ipk_min');
          $table->integer('ipk_avg');
          $table->integer('ipk_mak');
          $table->string('persen_kurang'); // persentase < 2,75
          $table->string('persen_tengah'); //persentase 2,75 - 3,5
          $table->string('persen_lebih'); //persentase > 3.5
          $table->unsignedInteger('standar3_id');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('mhs_reguler_tigas'))
        {
          Schema::drop('mhs_reguler_tigas');
        }
    }
}
