<?php

namespace App\Http\Controllers;

use App\Http\Controllers\MasterController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class userController extends Controller
{
    public function __construct()
    {
      $this->middleware('auth');
    }

    public function index()
    {
      $users = new User();
      return view('user/index')->with('users', $users->get());
    }

    public function insert(Request $req)
    {
      $user = new User();

      $user->name = $req->input('name');
      $user->email = $req->input('email');
      $user->password = hash::make($req->input('password'));
      $user->nidn = $req->input('nidn');
      $user->jabatan = $req->input('jabatan');
      $user->role = $req->input('role');

      $user->save();
      return redirect('user/index');
    }

    public function delete($id)
    {
      $user = User::find($id);
      $user->delete();

      return redirect('user/index');
    }
}