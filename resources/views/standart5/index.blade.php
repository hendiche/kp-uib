@extends('layouts.app')

@section('content')
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>{{ config('app.name', 'Laravel') }}</title>

	<!-- Styles -->
	<link href="/css/app.css" rel="stylesheet">

	<!-- Scripts -->
</head>

<body>
	<div class="container">
		<div class="panel panel-default">
			<div class="panel panel-heading">
				<a href="#">
					<span>Standart 5</span>
				</a>
			</div>
			<div class="panel-body">
				<!-- disabled temporary -->
				<!-- {!! Form::open(['url' => '#', 'method' => 'post']) !!} -->
				<table class="table">
					<tr>
						<td>
							<div>
								{{ Form::label('kompetensi', 'Kurikulum') }} <br/>
								{{ Form::label('kompetensi','Kurikulum pendidikan tinggi adalah seperangkat rencana dan pengaturan mengenai isi, bahan kajian, maupun bahan pelajaran serta cara penyampaiannya, dan penilaian yang digunakan sebagai pedoman penyelenggaraan kegiatan pembelajaran di perguruan tinggi.
								Kurikulum seharusnya memuat standar kompetensi lulusan yang terstruktur dalam kompetensi utama, pendukung lainnya yang mendukung tercapainya tujuan, terlaksananya misi, dan terwujudnya visi program studi. Kurikulum memuat mata kuliah/modul/blok yang mendukung pencapaian kompetensi lulusan dan memberikan keleluasaan pada mahasiswa untuk memperluas wawasan dan memperdalam keahlian sesuai dengan minatnya, serta dilengkapi dengan deskripsi mata kuliah/modul/blok, silabus, rencana pembelajaran dan evaluasi.
								Kurikulum harus dirancang berdasarkan relevansinya dengan tujuan, cakupan dan kedalaman materi, pengorganisasian yang mendorong terbentuknya hard skills dan keterampilan kepribadian dan perilaku (soft skills) yang dapat diterapkan dalam berbagai situasi dan kondisi.') }} <br/>
								{{ Form::label('kompetensi', 'Kompetensi') }}<br/>

								{{ Form::label('kompetensi', 'Uraikan secara ringkas kompetensi utama lulusan') }}
								<div>{{ Form::textarea('kompetensi', null, ['class' => 'textarea' ]) }}</div>
								<!-- <div>{{ Form::textarea('kompetensi', null, ['required' => '', 'class' => 'textarea' ]) }}</div> -->

								{{ Form::label('kompetensi', 'Uraikan secara ringkas kompetensi pendukung lulusan') }}
								<div>{{ Form::textarea('kompetensi', null, ['class' => 'textarea' ]) }}</div>
								<!-- <div>{{ Form::textarea('kompetensi', null, ['required' => '', 'class' => 'textarea' ]) }}</div> -->

								{{ Form::label('kompetensi', 'Uraikan secara ringkas kompetensi lainnya/pilihan lulusan') }}
								<div>{{ Form::textarea('kompetensi', null, ['class' => 'textarea' ]) }}</div>
								<!-- <div>{{ Form::textarea('kompetensi', null, ['required' => '', 'class' => 'textarea' ]) }}</div> -->

								<p class="catatan">
									Catatan: Pengertian tentang kompetensi utama, pendukung, dan lainnya dapat dilihat pada Kepmendiknas No. 045/2002.
								</p>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							{{ Form::label('', 'Struktur Kurikulum') }}
							<br/>
							{{ Form::label('', 'Jumlah sks PS (minimum untuk kelulusan):') }}
							<!-- {{ Form::text('sks_ps', null, ['required' => '']) }} -->
							{{ Form::text('sks_ps', null, []) }}
							{{ Form::label('', 'sks yang tersusun sebagai berikut:') }}
							<br/>
							<div id = "table1" class ="alpha">
								<table>
									<thead>
										<tr>
											<th width="200">Jenis Mata Kuliah</th>
											<th width="300">sks</th>
											<th width="400">Keterangan</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>Matakuliah Wajib</td>
											<td><input  id = "sks1" type = "number" style="width: 300px;"></input></td>
											<td><input style= "width: 400px;"></input></td>
										</tr>
										<tr>
											<td>Matakuliah Pilihan</td>
											<td><input id = "sks2" type = "number" style="width: 300px;"></input></td>
											<td><input style="width: 400px;"></input></td>
										</tr>
										<tr>
											<td>Matakuliah Total</td>
											<td ><input id = "totalMatakuliah" style = "width:300px;" readonly=""></input></td>
											<td style="background:#808080"></td>
										</tr>
									</tbody>
								</table>
							</div>
							<script>
								var total1 = 0,total2 = 0,total = 0;
								$(document).ready( function () {
									$('#sks1, #sks2').on('input', function() {
										total1 = $('#sks1').val();
										total2 = $('#sks2').val();
										itotal1 = parseInt(total1);
										itotal2 = parseInt(total2);
										if(itotal1 && itotal2){total = itotal1+itotal2;}
										else if (itotal2){total = itotal2;}
										else if (itotal1){total = itotal1;}
										else {total = 0;}
										$('#totalMatakuliah').val(total);
									});
								});
							</script>
						</td>
					</tr>

					<tr>
						<td>
							<div>
								{{ Form::label('', 'Tuliskan struktur kurikulum berdasarkan urutan mata kuliah(MK) semester demi semester, dengan mengikuti format tabel berikut:') }}<br/>
								<div id = "table2" class ="alpha">
									<table>
										<thead>
											<tr>
												<td rowspan="2">smt</td>
												<td rowspan="2">Kode MK</td>
												<td rowspan="2">Nama Mata Kuliah</td>
												<td rowspan="2">Bobot sks</td>
												<td colspan = "2">sks MK dalam Kurikulum</td>
												<td rowspan="2">Bobot tugas***</td>
												<td colspan = "3">Kelengkapan**** </td>
												<td rowspan="2">Unit/ Jur/ Fak Penyelenggara</td>
											</tr>
											<tr>										
												<td>inti**</td>
												<td>Institutional</td>
												<td>Deskripsi</td>
												<td>Silabus</td>
												<td>SAP</td>
											</tr>
										</thead>
										<tbody>
											<!-- insert data here -->
											<tr>
												<td>test</td>
												<td>test</td>
												<td>test</td>	
												<td>test</td>											
												<td>test</td>
												<td>test</td>
												<td>test</td>
												<td>test</td>
												<td>test</td>
												<td>test</td>
												<td>test</td>
											</tr>
										</tbody>
									</table>
								</div>
								<p class="catatan">
									* Tuliskan mata kuliah pilihan sebagai mata kuliah pilihan 1, mata kuliah pilihan 2, dst. (nama-nama mata kuliah pilihan yang dilaksanakan dicantumkan)<br/>
									** Menurut rujukan peer group / SK Mendiknas 045/2002 (ps. 3 ayat 2e)<br/>
									*** Beri tanda √ pada mata kuliah yang dalam penentuan nilai akhirnya memberikan bobot pada tugas-tugas (praktikum/praktek, PR atau makalah) ≥ 20% <br/>
									**** Beri tanda √ pada mata kuliah yang dilengkapi dengan deskripsi, silabus, dan atau SAP. Sediakan dokumen pada saat asesmen lapangan.
								</p>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<div>
								{{ Form::label('', 'Tuliskan mata kuliah yang dilaksanakan dalam tiga tahun terakhir, pada tabel berikut') }}<br/>
								<span style = "display:inline-block">
									<div id = "table3" class ="alpha" style = "display:inline-block">
										<form action = "{{ route('standart5.add') }}" method = "POST">
											{{ csrf_field() }}
											<table>
												<thead>
													<tr>
														<td width = "50">Semester</td>
														<td width = "150">Kode MK</td>
														<td width = "250">Nama MK (Pilihan)</td>
														<td width = "100">Bobot sks</td>
														<td width = "100">Bobot tugas</td>
														<td width = "200">Unit/ Jur/ Fak Pengelola</td>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>
															<select name = "semester[0]">
																<option value="1">1</option>
																<option value="2">2</option>
																<option value="3">3</option>
																<option value="4">4</option>
																<option value="5">5</option>
																<option value="6">6</option>
																<option value="7">7</option>
															</select>
														</td>
														<td>
															<input name = "KodeMK[0]" style = "width:150px"/>
														</td>											
														<td>
															<input  name = "namaMK[0]" style = "width:250px"/>
														</td>
														<td><input type = "number" name = "bobotSKS[0]" class = "bobotSKS" style = "width:100px"/></td>
														<td><input name = "bobotTugas[0]" style = "width:100px"/></td>
														<td><input name = "unit[0]" style = "width:200px"/></td>
													</tr>
													<tr>
														<td colspan="3">TotalSKS</td>
														<td><input name = "total_BobotSKS" id = "total_BobotSKS" style = "width:100px" readonly value=0></td>											
														<td style = "background:#808080"></td>
														<td style = "background:#808080"></td>
													</tr>
												</tbody>
											</table>
											<input type = "submit" value = "submit ke database (sementara)"/>
										</form>
									</div>
									<span style = "display:inline-block">
										<input id = "table3_addrow" type = "button" class= "btn-primary" style = "float:right" value = "Tambah Baris">
										<input id = "table3_removerow" type = "button" class= "btn-primary" style = "float:right" value = "Hapus Baris">

									</span>
									<!-- the script for table3 -->
									<script>
										i=0;
										$(document).ready( function () {
											$("#table3_removerow").hide();
											$(document.body).on('change', '.bobotSKS' ,function(){
												total=0;
												$('.bobotSKS').each(function(){
													value 	= parseInt($(this).val());
													total	+= value;
												});
												$('#total_BobotSKS').val(total);
											});

											$("#table3_addrow").click(function(){
												//add new row to table3
												i++;
												if(i != 0) {$("#table3_removerow").show();}
												$('#table3 tr:last').before("<tr>"+
													"<td>"+
													"<select name = 'semester["+i+"]'>"+
													"<option value='1'>1</option>"+
													"<option value='2'>2</option>"+
													"<option value='3'>3</option>"+
													"<option value='4'>4</option>"+
													"<option value='5'>5</option>"+
													"<option value='6'>6</option>"+
													"<option value='7'>7</option>"+
													"</select>"+
													"</td>"+
													"<td>"+
													"<input name = 'kodeMK["+i+"]' style = 'width:150px'/>"+
													"</td>"+											
													"<td>"+
													"<input  name = 'namaMK["+i+"]' style = 'width:250px'/>"+
													"</td>"+
													"<td><input type ='number' name = 'bobotSKS["+i+"]' class = 'bobotSKS' style = 'width:100px'/></td>"+
													"<td><input name = 'bobotTugas["+i+"]' style = 'width:100px'/></td>"+
													"<td><input name = 'unit["+i+"]' style = 'width:200px'/></td>"+
													"</tr>");
											});
											$("#table3_removerow").click(function(){
												//remove last row from table3
												$('#table3 tr:eq('+i+')').remove();
												i--;
												if(i == 0) {$("#table3_removerow").hide();}
											});

										});
									</script>



								</span>
								<p class="catatan">
									* beri tanda √ pada mata kuliah yang dalam penentuan nilai akhirnya memberikan bobot pada tugas-tugas (praktikum/praktek, PR atau makalah) ≥ 20%
								</p>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<div>
								{{ Form::label('', 'Tuliskan substansi praktikum/praktek yang mandiri ataupun yang merupakan bagian dari mata kuliah tertentu, dengan mengikuti format di bawah ini') }}<br/>
								<div id = "table4" class ="alpha">
									<table>
										<thead>
											<tr>
												<td rowspan = "2">No.</td>
												<td rowspan = "2">Nama Praktikum/Praktek</td>
												<td colspan = "2">Isi Praktikum/Praktek</td>
												<td rowspan = "2">Tempat/Lokasi Praktikum/Praktek</td>
											</tr>
											<tr>
												<td>Judul/Modul</td>
												<td>Jam Pelaksanaan</td>
											</tr>
										</thead>
										<tbody>

											<tr>
												<td>1</td>
												<td>test</td>								
												<td>test</td>
												<td>test</td>
												<td>test</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<div>
								{{ Form::label('', 'Peninjauan Kurikulum dalam 5 Tahun Terakhir') }}
								{{ Form::label('', 'Jelaskan mekanisme peninjauan kurikulum dan pihak-pihak yang dilibatkan dalam proses peninjauan tersebut.') }}<br/>
								{{ Form::textarea('standart5', null, ['class' => 'textarea']) }}<br/>
								<!-- {{ Form::textarea('standart5', null, ['required' => '', 'class' => 'textarea']) }}<br/> -->

								{{ Form::label('', 'Tuliskan hasil peninjauan tersebut, mengikuti format tabel berikut:') }}<br/>
								<div id = "table5" class ="alpha">
									<table>
										<thead>
											<tr>
												<td rowspan = "2">No.</td>
												<td rowspan = "2">No. MK</td>
												<td colspan = "2">Nama MK</td>
												<td rowspan = "2">MK Baru/Lama/Hapus</td>
												<td rowspan = "2">Perubahan Pada</td>
												<td rowspan = "2">Alasan Peninjauan</td>
												<td rowspan = "2">Atas Usulan/Masukan Dari</td>
												<td rowspan = "2">Berlaku Mulai Sem./Th.</td>
											</tr>
											<tr>
												<td>Silabus/SAP</td>
												<td>Buku Ajar</td>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>1</td>
												<td>test</td>
												<td>test</td>								
												<td>test</td>
												<td>test</td>
												<td>test</td>							
												<td>test</td>
												<td>test</td>
												<td>test</td>
											</tr>
										</tbody>
									</table>
								</div>

							</div>
						</td>
					</tr>
					<tr>
						<td>
							<div>
								{{ Form::label('', 'Pelaksanaan Proses Pembelajaran') }}
								{{ Form::label('', 'Sistem pembelajaran dibangun berdasarkan perencanaan yang relevan dengan tujuan, ranah belajar dan hierarkinya. Pembelajaran dilaksanakan menggunakan berbagai strategi dan teknik yang menantang, mendorong mahasiswa untuk berpikir untuk berpikir kritis bereksplorasi, berkreasi dam bereksperimen dengan memanfaatkan anueka sumber. Pelaksanaan pembelajaran memiliki mekanisme untuk memonitor, mengkaji dan memperbaiki secara periodik kegiatan perkuliahan (kehadiran dosen dan mahasiswa), penyusunan materi perkuliahan, serta penilaian hasil belajar.') }}<br/>
								{{ Form::label('', 'Mekanisme penyusunan materi kuliah dan monitoring perkuliahan') }}<br/>
								{{ Form::label('', 'Jelaskan mekanisme penyusunan materi kuliah dan monitoring perkuliahan, antara lain kehadiran kehadiran dosen dan mahasiswa, serta materi kuliah') }}<br/>
								<div>{{ Form::textarea('standart5', null, ['class' => 'textarea' ]) }}</div>
								<!-- <div>{{ Form::textarea('standart5', null, ['required' => '', 'class' => 'textarea' ]) }}</div> -->

							</div>
						</td>
					</tr>
					<tr>
						<td>
							<div>
								{{ Form::label('', 'Lampirkan contoh soal ujian dalam 1 tahun terakhir untuk 5 mata kuliah keahlian berikut silabusnya') }}<br/>
								{{ Form::label('', 'Sistem Pembimbingan Akademik') }}<br/>
								{{ Form::label('', 'Tuliskan nama dosen pembimbing akademik dan jumlah mahasiswa yang dibimbingnya dengan mengikuti format tabel berikut:') }}<br/>

								<div id = "table6" class ="alpha">
									<table>
										<thead>
											<tr>
												<td>No.</td>
												<td>Nama Dosen Pembingbing Akademik</td>
												<td>Jumlah Mahasiswa Bimbingan</td>
												<td>Rata-rata Banyaknya Pertemuan/mhs/semester</td>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>1</td>
												<td>test</td>
												<td>test</td>								
												<td>test</td>
											</tr>
											<tr>
												<td colspan = 2>Total</td>
												<td>test</td>
												<td style = "background:#808080"></td>
											</tr>
											<tr>
												<td colspan = 4>Rata-rata banyaknya pertemuan per mahasiswa per semester = 
													<input readonly placeholder="..."></input>
													kali</td>
												</tr>
											</tbody>
										</table>
									</div>


								</div>
							</td>
						</tr>
						<tr>
							<td>
								<div>
									{{ Form::label('', 'Jelaskan proses pembimbingan akademik yang diterapkan pada Program Studi ini') }}<br/>
									<div id = "table7" class ="alpha">
										<table>
											<thead>
												<tr>
													<td>No.</td>
													<td>Hal</td>
													<td width = "400">Penjelasan</td>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>1</td>
													<td>Tujuan Pembingbingan</td>
													<td><input id = "tujuan_pembingbingan" style = "width:400px"></input></td>
												</tr>
												<tr>
													<td>2</td>
													<td>Pelaksanaan Pembimbingan</td>
													<td><input id = "pelaksanaan_pembingbingan" style = "width:400px"></input></td>
												</tr>
												<tr>
													<td>3</td>
													<td>Masalah yang dibicarakan dalam pembimbingan</td>
													<td><input id = "masalah_pembingbingan" style = "width:400px"></input></td>
												</tr>
												<tr>
													<td>4</td>
													<td>Kesulitan dalam pembimbingan dan upaya untuk mengatasinya</td>
													<td><input id = "kesulitan_pembingbingan" style = "width:400px"></input></td>
												</tr>
												<tr>
													<td>5</td>
													<td>Manfaat yang diperoleh Mahasiswa dari pembimbingan</td>
													<td><input id = "manfaat_pembingbingan" style = "width:400px"></input></td>
												</tr>
											</tbody>
										</table>
									</div>							
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<div>
									{{ Form::label('', 'Pembimbingan tugas akhir/skripsi') }}<br/>
									<p class="bold">
										Rata-rata banyaknya mahasiswa per dosen pembimbing tugas akhir(TA)
										<!-- {{ Form::text('mahasiswa_bimbing', null, ['required' => '']) }} -->
										{{ Form::text('mahasiswa_bimbing', null, []) }}
										mahasiswa/dosen TA.<br/>
										Rata-rata jumlah pertemuan dosen-mahasiswa untuk menyelesaikan tugas akhir :
										{{ Form::text('pertemuan_skripsi', null, []) }}
										<!-- {{ Form::text('pertemuan_skripsi', null, ['required' => '']) }} -->
										kali mulai dari saat mengambil TA hingga menyelesaikan TA.<br/>
										Tuliskan nama-nama dosen yang menjadi pembimbing tugas akhir atau skripsi, dan jumlah mahasiswa yang bimbingan dengan mengikuti format tabel berikut:
									</p>
									<div id = "table8" class ="alpha">
										<table>
											<thead>
												<tr>
													<td>No.</td>
													<td width = "500">Nama Dosen Pembimbing</td>
													<td>Jumlah Mahasiswa</td>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>1</td>
													<td>input</td>
													<td>input</td>
												</tr>
											</tbody>
										</table>
									</div>						

									{{ Form::label('', 'Ketersediaan panduan pembimbingan tugas akhir (Beri tanda √ pada pilihan yang sesuai):') }}
									<div> {{ Form::radio('choose', 'ya') }}Ya</div>
									<div>
										{{ Form::radio('choose', 'tidak') }}Tidak</div>
										<div> {{ Form::label('', 'Jelaskan cara sosialisasi dan pelaksanaannya.') }}</div>
										<div> {{ Form::textarea('bimbingan_skripsi', null, ['class' => 'textarea' ]) }}</div>
										<!-- <div> {{ Form::textarea('bimbingan_skripsi', null, ['required' => '', 'class' => 'textarea' ]) }}</div> -->
									</div>
								</td>
							</tr>
							<tr>
								<td>
									<div>
										<p class="bold">
											Rata-rata lama penyelesaian tugas akhir/skripsi pada tiga tahun terakhir: 
											<!-- {{ Form::text('lama_skripsi', null, ['required' => '']) }} bulan.<br/> -->
											{{ Form::text('lama_skripsi', null, []) }} bulan.<br/>
											(Menurut kurikulum tugas akhir direncanakan {{ Form::text('kurikulum_skripsi', null, []) }} <!-- {{ Form::text('kurikulum_skripsi', null, ['required' => '']) }} --> semester).
										</p>
									</div>
								</td>
							</tr>
							<tr>
								<td>
									<div>
										{{ Form::label('', 'Upaya Perbaikan Pembelajaran') }}<br/>
										{{ Form::label('', 'Uraikan upaya perbaikan pembelajaran serta hasil yang telah dilakukan dan dicapai dalam tiga tahun terakhir dan hasilnya.') }}<br/>
										<div id = "table9" class ="alpha">
											<table>
												<thead>
													<tr>
														<td rowspan = "2">Butir</td>
														<td colspan = "2" width = "700">Upaya Perbaikan</td>
													</tr>
													<tr>
														<td>Tindakan</td>
														<td>Hasil</td>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>Materi</td>
														<td>test</td>
														<td>test</td>
													</tr>
													<tr>
														<td>Metodi Pembelajaran</td>
														<td>test</td>
														<td>test</td>
													</tr>
													<tr>
														<td>Penggunaan Teknologi Pembelajaran</td>
														<td>test</td>
														<td>test</td>
													</tr>
													<tr>
														<td>Cara-cara Evaluasi</td>
														<td>test</td>
														<td>test</td>
													</tr>


												</tbody>
											</table>
										</div>						
									</div>
								</td>
							</tr>
							<tr>
								<td>
									<div>
										{{ Form::label('', 'Upaya Peningkatan Suasana Akademik') }}<br/>
										{{ Form::label('', 'Berikan gambaran yang jelas mengenai upaya dan kegiatan untuk menciptakan suasana akademik yang kondusif di lingkungan PS, khususnya mengenai hal-hal berikut:') }}
										{{ Form::label('', 'Kebijakan tentang suasana akademik (otonomi keilmuan, kebebasan akademik, kebebasan mimbar akademik).') }}<br/>
										<div> {{ Form::textarea('standart5', null, ['class' => 'textarea' ]) }}</div>
										<!-- <div> {{ Form::textarea('standart5', null, ['required' => '', 'class' => 'textarea' ]) }}</div> -->

									</div>
								</td>
							</tr>
							<tr>
								<td>
									<div>
										{{ Form::label('', 'Ketersediaan dan jenis prasarana, sarana dan dana yang memungkinkan terciptanya interaksi akademik antara sivitas akademika.') }}<br/>
										<div> {{ Form::textarea('', null, ['class' => 'textarea' ]) }}</div>
										<!-- <div> {{ Form::textarea('', null, ['required' => '', 'class' => 'textarea' ]) }}</div> -->

									</div>
								</td>
							</tr>
							<tr>
								<td>
									<div>
										{{ Form::label('', 'Program dan kegiatan di dalam dan di luar proses pembelajaran, yang dilaksanakan baik di dalam maupun di luar kelas,  untuk menciptakan suasana akademik yang kondusif (misalnya seminar, simposium, lokakarya, bedah buku, penelitian bersama, pengenalan kehidupan kampus, dan temu dosen-mahasiswa-alumni).') }}<br/>
										<div> {{ Form::textarea('', null, ['class' => 'textarea' ]) }}</div>
										<!-- <div> {{ Form::textarea('', null, ['required' => '', 'class' => 'textarea' ]) }}</div> -->

									</div>
								</td>
							</tr>
							<tr>
								<td>
									<div>
										{{ Form::label('', 'Interaksi akademik antara dosen-mahasiswa, antar mahasiswa, serta antar dosen.') }}<br/>
										<div> {{ Form::textarea('', null, ['class' => 'textarea' ]) }}</div>
										<!-- <div> {{ Form::textarea('', null, ['required' => '', 'class' => 'textarea' ]) }}</div> -->

									</div>
								</td>
							</tr>
							<tr>
								<td>
									<div>
										{{ Form::label('', 'Pengembangan perilaku kecendekiawanan.') }}<br/>
										<div> {{ Form::textarea('', null, ['class' => 'textarea' ]) }}</div>
										<!-- <div> {{ Form::textarea('', null, ['required' => '', 'class' => 'textarea' ]) }}</div> -->

									</div>
								</td>
							</tr>

							<tr>
								<td colspan="2">
									{{ Form::submit('confirm', ['class' => 'btn btn-info']) }}
									<a href="{{ url('home') }}" class="btn btn-warning">Back</a>
								</td>
							</tr>
						</table>
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</body>

		</html>
		@endsection