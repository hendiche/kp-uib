<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AksesibilitasSixes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aksesibilitas', function(Blueprint $table){
            $table->increments('id');
            $table->string('jenis');
            $table->string('manual');
            $table->string('tanpa_jaringan');
            $table->string('lan');
            $table->string('wan');
            $table->unsignedInteger('standar6_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('aksesibilitas'))
        {
            Schema::drop('aksesibilitas');
        }
    }
}
