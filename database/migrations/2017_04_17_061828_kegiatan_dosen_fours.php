<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class KegiatanDosenFours extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kegiatan_dosen_empats', function(Blueprint $table) {
          $table->increments('id');
          $table->string('nama');
          $table->string('jenis');
          $table->string('tempat');
          $table->time('waktu');
          $table->string('penyaji');
          $table->string('peserta');
          $table->unsignedInteger('standar4_id');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('kegiatan_dosen_empats'))
        {
          Schema::drop('kegiatan_dosen_empats');
        }
    }
}
