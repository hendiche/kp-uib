@extends('layouts.app')

@section('content')
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>{{ config('app.name', 'Laravel') }}</title>

	<!-- Styles -->
	<link href="/css/app.css" rel="stylesheet">

	<!-- Scripts -->
</head>

<body>
	<div class="container">
		<div class="panel panel-default">
			<div class="panel panel-heading">
			<a href="#">
				<span>Standart 7</span>
			</a>
			</div>
			<div class="panel-body">
				{!! Form::open(['url' => '#', 'method' => 'post']) !!}
				<table class="table">
					<tr>
						<td>
						<div>
						{{ Form::label('', 'Penelitian Dosen Tetap yamg Bidang Keahliannya Sesuai dengan PS') }}<br/>
						{{ Form::label('', 'Tuliskan jumlah judul penelitian* yang sesuai dengan bidang keilmuan PS, yang dilakukan oleh dosen tetap yang bidang keahliannya sesuai dengan PS selama tiga tahun terakhir dengan mengikuti format tabel berikut:') }}<br/>
						<button type="button" class="btn btn-info">Button</button><br/>
						<p class="catatan">
						Catatan: (*) sediakan data pendukung pada saat asesmen lapangan
						</p>
						</div>
						</td>
					</tr>
					<tr>
						<td>
						<div>
						{{ Form::label('', 'Adakah mahasiswa tugas akhir yang dilibatkan dalam penelitian dosen dalam tiga tahun terakhir?  ') }}
						<div> {{ Form::radio('choose', 'tidak', true) }}Tidak Ada</div>
						<div> {{ Form::radio('choose', 'ya') }}Ya</div>
						<p class="bold">
						Banyaknya mahasiswa PS yang ikut serta dalam penelitian dosen adalah {{ Form::text('penelitian_dosen', null, ['required' => '']) }} orang, dari {{ Form::text('jml_penelitian_dosen', null, ['required' => '']) }} mahasiswa yang melakukan tugas akhir melalui skripsi
						</p>
						</div>
						</td>
					</tr>
					<tr>
						<td>
						<div>
						{{ Form::label('', 'Tuliskan judul artikel ilmiah/karya ilmiah/karya seni/buku yang dihasilkan selama tiga tahun terakhir oleh dosen tetap yang bidang keahliannya sesuai dengan PS dengan mengikuti format tabel berikut:') }}<br/>
						<button type="button" class="btn btn-info">Button</button><br/>
						<p class="catatan">
						Catatan *= Tuliskan banyaknya dosen pada sel yang sesuai
						</p>
						</div>
						</td>
					</tr>
					<tr>
						<td>
						<div>
						{{ Form::label('', 'Sebutkan karya dosen dan atau mahasiswa Program Studi yang telah memperoleh/sedang memproses perlindungan Hak atas Kekayaan Intelektual (HaKI) selama tiga tahun terakhir.') }}<br/>
						<button type="button" class="btn btn-info">Button</button><br/>
						<p class="catatan">
						* Lampirkan surat paten HaKI atau keterangan sejenis
						</p>
						</div>
						</td>
					</tr>
					<tr>
						<td>
						<div>
						{{ Form::label('', 'Kegiatan Pelayanan/Pengabdian kepada Masyarakat(PkM)') }}<br/>
						{{ Form::label('', 'Tuliskan jumlah kegiatan Pelayanan/Pengabdian kepada Masyarakat (*) yang sesuai dengan bidang keilmuan PS selama tiga tahun terakhir yang dilakukan oleh dosen tetap yang bidang keahliannya sesuai dengan PS dengan mengikuti format tabel berikut:') }}<br/>
						<button type="button" class="btn btn-info">Button</button><br/>
						<p class="catatan">
						Catatan: (*) Pelayanan/Pengabdian kepada Masyarakat adalah penerapan bidang ilmu untuk menyelesaikan masalah di masyarakat (termasuk masyarkat industri, pemerintah, dsb.)
						</p>
						</div>
						</td>
					</tr>
					<tr>
						<td>
						<div>
						{{ Form::label('', 'Adakah mahasiswa yang dilibatkan dalam kegiatan pelayanan/pengabdian kepada masyarakat dalam tiga tahun terakhir?') }}<br/>
						<div> {{ Form::radio('choose', 'tidak', true) }}Tidak</div>
						<div> {{ Form::radio('choose', 'ya') }}Ya</div>
						{{ Form::label('', 'Jelaskan tingkat partisipasi dan bentuk keterlibatan mahasiswa dalam kegiatan pelayanan/pengabdian kepada masyarakat.') }}<br/>
						<div> {{ Form::textarea('', null, ['required' => '', 'class' => 'textarea' ]) }}</div>
						</div>
						</td>
					</tr>
					<tr>
						<td>
						<div>
						{{ Form::label('', 'Kegiatan Kerjasama dengan Instansi Lain') }}<br/>
						{{ Form::label('', 'Tuliskan instansi dalam negeri yang menjalin kerjasama* yang terkait dengan program studi/jurusan dalam tiga tahun terakhir.') }}<br/>
						<button type="button" class="btn btn-info">Button</button><br/>
						<p class="catatan">
						Catatan: (*) dokumen pendukung disediakan pada saat asesmen lapangan
						</p>
						</div>
						</td>
					</tr>
					<tr>
						<td>
						<div>
						{{ Form::label('', 'Tuliskan instansi luar negeri yang menjalin kerja sama* tekait dengan program studi/jurusan dalam tiga tahun terakhir.') }}<br/>
						<button type="button" class="btn btn-info">Button</button><br/>
						<p class="catatan">
						Catatan: (*) dokumen pendukung disediakan pada saat asesmen lapangan
						</p>
						</div>
						</td>
					</tr>

					<tr>
						<td colspan="2">
						{{ Form::submit('confirm', ['class' => 'btn btn-info']) }}
						<a href="{{ url('home') }}" class="btn btn-warning">Back</a>
						</td>
					</tr>

				</table>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</body>

</html>
@endsection