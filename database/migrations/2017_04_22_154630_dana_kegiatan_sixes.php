<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DanaKegiatanSixes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dana_kegiatans', function(Blueprint $table){
            $table->increments('id');
            $table->date('tahun');
            $table->string('judul');
            $table->string('sumber');
            $table->integer('dana');
            $table->unsignedInteger('standar6_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('dana_kegiatans'))
        {
            Schema::drop('dana_kegiatans');
        }
    }
}
