<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AktivitasDosenTetapFours extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aktivitas_dosen_tetap_empats', function(Blueprint $table) {
          $table->increments('id');
          $table->string('nama_dosen');
          $table->integer('ngajar_ps_sndri');
          $table->integer('ngajar_ps_lain_pt_sndri');
          $table->integer('ngajar_pt_lain');
          $table->integer('penelitian');
          $table->integer('pengabdian');
          $table->integer('sks_manajemen_sndri');
          $table->integer('sks_manajemen_lain');
          $table->integer('jumlah');
          $table->unsignedInteger('standar4_id');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      if (Schema::hasTable('aktivitas_dosen_tetap_empats'))
      {
        Schema::drop('aktivitas_dosen_tetap_empats');
      }
    }
}
