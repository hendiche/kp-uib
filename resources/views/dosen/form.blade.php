@extends('layouts.app')

@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-md-2 col-sm-2 noPadding">
      @include('layouts.navigation')
    </div>
    <div class="col-md-10 col-sm-10 noPadding">
      <div class="portlet-title">
        <div class="caption">
          Create form - Dosen
        </div>
      </div>
      <div class="portlet-body">
        {!! Form::model($model, [
          'url' => $route,
          'method' => $method,
          'id' => 'userForm',
        ]) !!}

        <div class="form-body">
          <table class="table">
            <tr>
              <td>{{ Form::label('', 'Nama') }}</td>
              <td>{{ Form::text('nama', null, ['required', 'class' => 'sizeBox']) }}</td>
            </tr>
            <tr>
              <td>{{ Form::label('', 'NIDN') }}</td>
              <td>{{ Form::text('nidn', null, ['required', 'class' => 'sizeBox']) }}</td>
            </tr>
            <tr>
              <td>{{ Form::label('', 'Tanggal Lahir') }}</td>
              <td>{{ Form::text('tgl_lahir', null, ['id' => 'tgl_lahir', 'class' => 'sizeBox', 'readonly']) }}</td>
              {{-- <td>{{ Form::date('tgl_lahir', \Carbon\Carbon::now()) }}</td> --}}
            </tr>
            <tr>
              <td>{{ Form::label('', 'Jabatan') }}</td>
              <td>{{ Form::text('jabatan', null, ['required', 'class' => 'sizeBox']) }}</td>
            </tr>
            <tr>
              <td>{{ Form::label('', 'Gelar') }}</td>
              <td>{{ Form::text('gelar', null, ['required', 'class' => 'sizeBox']) }}</td>
            </tr>
            <tr>
              <td>{{ Form::label('', 'Pendidikan') }}</td>
              <td>{{ Form::text('pendidikan', null, ['required', 'class' => 'sizeBox']) }}</td>
            </tr>
            <tr>
              <td>{{ Form::label('', 'Bidang Keahlian') }}</td>
              <td>{{ Form::text('bidang_keahlian', null, ['required', 'class' => 'sizeBox']) }}</td>
            </tr>
            <tr>
              <td>{{ Form::label('', 'Tipe Dosen') }}</td>
              <td>{{ Form::select('tipe_dosen', ['tetap' => 'Tetap', 'tdk_tetap' => 'Tidak Tetap'], null, ['class' => 'sizeBox selectHeight']) }}</td>
            </tr>
            <tr>
              <td colspan="2">
                <div class="form-actions text-right">
                  <button type="submit" class="btn btn-info">Save</button>
                </div>
              </td>
            </tr>
          </table>
        </div>
        {!! Form::close() !!}
      </div>
    </div>
  </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
  $(function() {
    $("#tgl_lahir" ).datepicker({
      changeMonth: true,
      changeYear: true,
      dateFormat: 'dd-MM-yy'
    });
  });
</script>
@endsection