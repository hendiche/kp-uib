@extends('layouts.app')

@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-md-2 col-sm-2 noPadding">
      @include('layouts.navigation')
    </div>
    <div class="col-md-10 col-sm-10 noPadding">
      <div class="text-right">
        <a class="btn btn-primary" href="{{ route('dosen.create') }}">Add New</a>
      </div>

      <table class="table" id="dosen-table">
        <thead>
          <tr role="row">
            <th>No</th>
            <th>Nama</th>
            <th>NIDN</th>
            <th>Tanggal Lahir</th>
            <th>Jabatan Akademik</th>
            <th>Gelar Akademik</th>
            <th>Pendidikan</th>
            <th>Bidang Keahlian</th>
            <th>Tipe Dosen</th>
            <th>Options</th>
          </tr>
        </thead>
      </table>
    </div>
  </div>
</div>

@endsection
@section('script')
<script type="text/javascript">
  $(document).ready(function() {
    var DataTable = $('#dosen-table').DataTable(
      {
      dom: 'lrtip',
      responsive: true,
      processing: true,
      serverSide: true,
      ajax: {
        url: '{{ route('dosen.datatable') }}',
        data: function(data) {
          data._token = '{{ csrf_token() }}'
        },
        type: 'POST',
      },
      columns: [
        { data: 'nama', name: 'nama' },
        { data: 'nidn', name: 'nidn' },
        { data: 'tgl_lahir', name: 'tgl_lahir' },
        { data: 'jabatan', name: 'jabatan' },
        { data: 'gelar', name: 'gelar' },
        { data: 'pendidikan', name: 'pendidikan' },
        { data: 'bidang_keahlian', name: 'bidang_keahlian' },
        { data: 'tipe_dosen', name: 'tipe_dosen' },
        { data: 'action', name: 'action', orderable: false, searchable: false }
      ]
    });
  })
</script>
@endsection