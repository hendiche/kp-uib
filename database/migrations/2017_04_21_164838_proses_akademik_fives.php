<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProsesAkademikFives extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proses_akademiks', function(Blueprint $table){
            $table->increments('id');
            $table->string('hal');
            $table->string('keterangan');
            $table->unsignedInteger('standar5_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('proses_akademiks'))
        {
            Schema::drop('proses_akademiks');
        }
    }
}
