<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlokasiDanaSixes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alokasi_danas', function(Blueprint $table){
            $table->increments('id');
            $table->string('jenis');
            $table->string('sumber_dana');
            $table->string('jenis_dana');
            $table->integer('ts-2');
            $table->integer('ts-1');
            $table->integer('ts');
            $table->unsignedInteger('standar6_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('alokasi_danas'))
        {
            Schema::drop('alokasi_danas');
        }
    }
}
