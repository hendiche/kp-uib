<?php

namespace App\Http\Controllers;

use App\Http\Controllers\MasterController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\prody_identities;
use App\Models\standard_one;
use App\Models\standard_two;
use App\Models\standard_three;
use App\Models\standard_four;
use App\Models\standard_five;
use App\Models\standard_six;
use App\Models\standard_seven;

class prodicontroller extends Controller
{
    protected $attributes = [
      'title' => '',
    ];

    protected $index_view = 'prodi.index';
    protected $form_view = 'prodi.form';
    protected $redirect_when_success = 'prodi.index';

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
      return view('prodi/index');
    }

    public function add(Request $req)
    {
      $this->attributes['route'] = route('item.store');
      $this->attributes['method'] = 'post';
      return $this->render(view($this->form_view));

      return redirect('prodi/index');
    }

    public function store(Request $req, prodi_identitas $prodi = null)
    {
      if (!$item) {
        $item = new prodi_identitas();
      }

      $std1 = new standard_one();
      $std2 = new standard_two();
      $std3 = new standard_three();
      $std4 = new standard_four();

      $prodi->program_studi = $req->input('prodi');
      $prodi->jurusan = $req->input('jurusan');
      $prodi->fakultas = $req->input('fakultas');
      $prodi->perguruan_tinggi = $req->input('perguruTinggi');
      $prodi->no_sk_prodi = $req->input('noSK');
      $prodi->tgl_sk_prodi = $req->input('tglSK');
      $prodi->pejabat_sk_prodi = $req->input('pejabatSK');
      $prodi->penyelenggara_prodi = $req->input('mulaiPS');
      $prodi->no_sk_izin = $req->input('noSKizin');
      $prodi->tgl_sk_izin = $req->input('tglSKizin');
      $prodi->peringkat_akreditas = $req->input('peringkat');
      $prodi->no_sk_banpt = $req->input('noSKBANPT');
      $prodi->alamat_prodi = $req->input('alamat');
      $prodi->no_telp_prodi = $req->input('noTelpPS');
      $prodi->no_faksimili_prodi = $req->input('noFaksimili');
      $prodi->email_prodi = $req->input('emailPS');
      $prodi->save();

      $prodi->get();

      $std1->prodi_id = $prodi->id;
      $std2->prodi_id = $prodi->id;
      $std3->prodi_id = $prodi->id;
      $std4->prodi_id = $prodi->id;
      $std1->save();
      $std2->save();
      $std3->save();
      $std4->save();

      return redirect()->route($this->redirect_when_success);
    }
}
