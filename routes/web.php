<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
// Route::get('/', function () {
//     return view('home');
// });

Route::get('/', 'HomeController@index');

Route::get('prodi/index', 'prodicontroller@index')->name('prodi.index');
  Route::post('prodi/insert', 'prodicontroller@add')->name('prodi.create');

Route::get('standart1/index', 'standart1controller@index');
  Route::post('standart1/insert', 'standart1controller@add');

Route::get('standart2/index', 'standart2controller@index');

Route::get('standart3/index', 'standart3controller@index');

Route::get('standart4/index', 'standart4controller@index');


Route::get('standart5/index', 'standart5controller@index');
  Route::post('standart5/insert', 'standart5controller@store')->name('standart5.add');



Route::get('standart6/index', 'standart6controller@index');

Route::get('standart7/index', 'standart7controller@index');

Route::get('user/index', 'userController@index');
  Route::post('user/add', 'userController@insert');
  Route::get('user/delete/{id}', 'userController@delete');


Route::group(['prefix' => 'dosen'], function () {
  Route::get('/index', 'dosenController@index')->name('dosen.index');
  Route::get('/create', 'dosenController@create')->name('dosen.create');
  Route::post('/store', 'dosenController@store')->name('dosen.store');
  Route::post('/data_table', 'dosenController@dataTable')->name('dosen.datatable');
});

Route::group(['prefix' => 'aktivitas_dosen'], function () {
  Route::get('/index', 'aktivitasDosenController@index')->name('aktivitas.index');
  Route::get('/create', 'aktivitasDosenController@create')->name('aktivitas.create');
  Route::post('/store', 'aktivitasDosenController@store')->name('aktivitas.store');
  Route::post('/data_table', 'aktivitasDosenController@dataTable')->name('aktivitas.datatable');
});