@extends('layouts.app')

@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-md-2 col-sm-2 noPadding">
      @include('layouts.navigation')
    </div>
    <div class="col-md-10 col-sm-10 noPadding">
      <div class="text-right">
        <a class="btn btn-primary" href="{{ route('dosen.create') }}">Add New</a>
      </div>

      <table class="table" id="dosen-table">
        <thead>
          <tr role="row">
            <th>No</th>
            <th>Nama</th>
            <th>Bidang Keahlian</th>
            <th>Kode Mata Kuliah</th>
            <th>Nama Mata Kuliah</th>
            <th>Jumlah Kelas</th>
            <th>Jumlah Pertemuan Direncanakan</th>
            <th>Jumlah Pertemuan Dilaksanakan</th>
            <th>Tipe Dosen</th>
            <th>Options</th>
          </tr>
        </thead>
      </table>
    </div>
  </div>
</div>

@endsection
@section('script')
<script type="text/javascript">
  $(document).ready(function() {
    var DataTable = $('#dosen-table').DataTable(
      {
      dom: 'lrtip',
      responsive: true,
      processing: true,
      serverSide: true,
      ajax: {
        url: '{{ route('dosen.datatable') }}',
        data: function(data) {
          data._token = '{{ csrf_token() }}'
        },
        type: 'POST',
      },
      columns: [
        { data: 'nama', name: 'nama' },
        { data: 'bidang', name: 'bidang' },
        { data: 'kode_matkul', name: 'kode_matkul' },
        { data: 'nama_matkul', name: 'nama_matkul' },
        { data: 'jumlah_kelas', name: 'jumlah_kelas' },
        { data: 'jumlah_direncanakan', name: 'jumlah_direncanakan' },
        { data: 'jumlah_terlaksana', name: 'jumlah_terlaksana' },
        { data: 'tipe_dosen', name: 'tipe_dosen' },
        { data: 'action', name: 'action', orderable: false, searchable: false }
      ]
    });
  })
</script>
@endsection