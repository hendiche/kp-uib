<?php

namespace App\Http\Controllers;

use App\Http\Controllers\MasterController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\prody_identities;
use App\Models\standard_one;

class standart1controller extends Controller
{
    public function __construct()
    {
    	$this->middleware('auth');
    }

    public function index()
    {
      $prodi = new prody_identities();
    	return view('standart1/index')->with('prodi', $prodi->get());
    }

    public function add(Request $req)
    {
      $std1 = new standard_one();

      $std1->mekanis_susun = $req->input('mekanisme');
      $std1->visi = $req->input('visi');
      $std1->misi = $req->input('misi');
      $std1->tujuan = $req->input('tujuan');
      $std1->sasaran_strategi = $req->input('sasaran');
      $std1->sosialisasi = $req->input('sosialisasi');
      $std1->prodi_id = $req->input('prodi');
      $std1->save();
      return redirect('standart1/index');
    }
}
