<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LayananMhsThrees extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('layanan_mhs_tigas', function(Blueprint $table) {
          $table->increments('id');
          $table->string('jenis_layanan');
          $table->string('hasil_kegiatan');
          $table->unsignedInteger('standar3_id');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('layanan_mhs_tigas'))
        {
          Schema::drop('layanan_mhs_tigas');
        }
    }
}
