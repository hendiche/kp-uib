@extends('layouts.app')

@section('content')
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>{{ config('app.name', 'Laravel') }}</title>

	<!-- Styles -->
	<link href="/css/app.css" rel="stylesheet">

	<!-- Scripts -->
</head>

<body>
	<div class="container">
		<div class="panel panel-default">
			<div class="panel panel-heading">
			<a href="#">
				<span>Standart 4</span>
			</a>
			</div>
			<div class="panel-body">
				{!! Form::open(['url' => '#', 'method' => 'post']) !!}
				<table class="table">
				<tr>
					<td>
					<div>
            {{ Form::label('', 'Sistem Seleksi dan Pengembangan') }} <br/>
            {{ Form::label('', 'Jelaskan sistem seleksi/perekrutan, penempatan, pengembangan, retensi, dan
            pemberhentian dosen dan tenaga kependidikan untuk menjamin mutu penyelenggaraan program akademik
            (termasuk informasi tentang ketersediaan pedoman tertulis dan konsistensi pelaksanaaannya).') }}
          </div>
					<div>{{ Form::textarea('seleksi', null, ['required' => '', 'class' => 'textarea' ]) }}</div>
					</td>
				</tr>
				<tr>
					<td>
					<div>
            {{ Form::label('', 'Monitoring dan Evaluasi') }} <br/>
            {{ Form::label('', 'Jelaskan sistem monitoring dan evaluasi, serta rekam jejak kinerja akademik
            dosen dan kinerja tenaga kependidikan (termasuk informasi tentang ketersediaan pedoman tertulis,
            an monitoring dan evaluasi kinerja dosen dalam tridarma serta dokumentasinya).') }}
          </div>
					<div>{{ Form::textarea('monitor', null, ['required' => '', 'class' => 'textarea' ]) }}</div>
					</td>
				</tr>
				<tr>
					<td>
					<div>
            {{ Form::label('', 'Dosen Tetap') }} <br/>
            {{ Form::label('', 'Dosen tetap dalam borang akreditasi BAN-PT adalah dosen yang diangkat dan
            ditempatkan sebagai tenaga tetap pada PT yang bersangkutan; termasuk dosen penugasan Kopertis,
            dan dosen yayasan pada PTS dalam bidang yang relevan dengan keahlian bidang studinya. Seorang dosen
            hanya dapat menjadi dosen tetap pada satu perguruan tinggi, dan mempunyai penugasan kerja minimum 36 jam/minggu.') }} <br/>
            {{ Form::label('', 'Dosen tetap dipilah dalam 2 kelompok, yaitu:') }} <br/>
            {{ Form::label('', '1. dosen tetap yang bidang keahliannya sesuai dengan PS') }} <Br/>
            {{ Form::label('', '2. dosen tetap yang bidang keahliannya di luar PS') }} </Br>
            </div>
					</td>
				</tr>
        <tr>
          <td>
            {{ Form::label('', 'Data dosen tetap yang bidang keahliannya seusai dengan bidang PS:') }} <br/>
            <button type="button" class="btn btn-info">Button</button>
          </td>
        </tr>
        <tr>
          <td>
            {{ Form::label('', 'Data dosen tetap yang bidang keahliannya di luar bidang PS:') }} <br/>
            <button type="button" class="btn btn-info">Button</button>
          </td>
        </tr>
        <tr>
          <td>
            <p class="bold">
              Aktivitas dosen tetap yang bidang bidang keahliannya sesuai dengan PS dinyatakan dalam
               <span class="bolder">sks rata-rata per semester</span>pada satu tahun akademik terakhir, diisi dengan perhitungan
               sesuai SK Dirjen DIKTI no. 48 tahun 1983 (12 sks setara dengan 36 jam kerja per minggu)
            </p>
            <p class="catatan">
            catatan: <br/>
            Sks pengajaran sama dengan sks mata kuliah yang diajarkan. Bila dosen mengajar kelas
            paralel, maka beban sks pengajaran untuk satu tambahan kelas paralel adalah 1/2 kali sks mata kuliah.<br/>
            *   rata-rata adalah jumlah sks dibagi dengan jumlah dosen tetap.<br/>
            **  sks manajemen dihitung sbb :<br/>
            Beban kerja manajemen untuk jabatan-jabatan ini adalah sbb.<Br/>
            - rektor/direktur politeknik 12 sks <br/>
            - pembantu rektor/dekan/ketua sekolah tinggi/direktur akademi 10 sks <Br/>
            - ketua lembaga/kepala UPT 8 sks <Br/>
            - pembantu dekan/ketua jurusan/kepala pusat/ketua senat akademik/ketua senat fakultas 6 sks <Br/>
            - sekretaris jurusan/sekretaris pusat/sekretaris senat akademik/sekretaris senat universitas/
            sekretaris senat fakultas/ kepala lab. atau studio/kepala balai/ketua PS 4 sks <Br/>
            - sekretaris PS 3 sks <br/>
            Bagi PT yang memiliki struktur organisasi yang berbeda, beban kerja manajemen untuk jabatan baru disamakan dengan beban kerja jabatan yang setara. <br/>
            </p>
          </td>
        </tr>
        <tr>
          <td>
            {{ Form::label('', 'Tuliskan data aktivitas mengajar dosen tetap yang bidang keahliannya
            sesuai dengan PS,  dalam satu tahun akademik terakhir di PS ini dengan mengikuti format
            tabel berikut:') }}<br/>
            <button type="button" class="btn btn-info">Button</button>
          </td>
        </tr>
        <tr>
          <td>
            {{ Form::label('', 'Tuliskan data aktivitas mengajar dosen tetap yang bidang keahliannya
             di luar PS,  dalam satu tahun akademik terakhir di PS ini dengan mengikuti format tabel
              berikut:') }} <br/>
              <button type="button" class="btn btn-info">Button</button>
          </td>
        </tr>
        <tr>
          <td>
            {{ Form::label('', 'Dosen Tidak Tetap') }} <Br/>
            {{ Form::label('', 'Tuliskan data dosen tidak tetap pada PS dengan mengikuti format tabel berikut:') }} <Br/>
            <button type="button" class="btn btn-info">Button</button>
          </td>
        </tr>
        <tr>
          <td>
            {{ Form::label('', 'Tuliskan data aktivitas mengajar dosen tidak tetap pada satu tahun terakhir di PS ini dengan mengikuti format tabel berikut:') }} <br/>
            <button type="button" class="btn btn-info">Button</button>
          </td>
        </tr>
        <tr>
          <td>
          {{ Form::label('', 'Upaya Peningkatan Sumber Daya Manusia (SDM) dalam tiga tahun terakhir') }} <br/>
          {{ Form::label('', 'Kegiatan tenaga ahli/pakar sebagai pembicara dalam seminar/pelatihan,
          pembicara tamu, dsb, dari luar PT sendiri (tidak termasuk dosen tidak tetap)') }}<br/>
          <button type="button" class="btn btn-info">Button</button>
          </td>
        </tr>
        <tr>
          <td>
            {{ Form::label('', 'Peningkatan kemampuan dosen tetap melalui program tugas belajar dalam bidang yang sesuai dengan bidang PS') }}<br/>
            <button type="button" class="btn btn-info">Button</button>
          </td>
        </tr>
        <tr>
          <td>
            {{ Form::label('', 'Kegiatan dosen tetap yang bidang keahliannya sesuai dengan PS dalam seminar
            ilmiah/lokakarya/penataran/workshop/ pagelaran/ pameran/peragaan yang tidak hanya melibatkan dosen
            PT sendiri') }}<br/>
            <button type="button" class="btn btn-info">Button</button><br/>
            <p class="catatan">
            * Jenis kegiatan : Seminar ilmiah, Lokakarya, Penataran/Pelatihan, Workshop, Pagelaran, Pameran, Peragaan dll
            </p>
          </td>
        </tr>
        <tr>
          <td>
            {{ Form::label('', 'Sebutkan pencapaian prestasi/reputasi dosen (misalnya prestasi dalam pendidikan,
            penelitian dan pelayanan/pengabdian kepada masyarakat).') }} <br/>
            <button type="button" class="btn btn-info">Button</button><Br/>
            <p class="catatan">
              * Sediakan dokumen pendukung pada saat asesmen lapangan.
            </p>
          </td>
        </tr>
        <tr>
          <td>
            {{ Form::label('', 'Sebutkan keikutsertaan dosen tetap dalam organisasi keilmuan atau organisasi profesi.') }} <br/>
            <button type="button" class="btn btn-info">Button</button>
          </td>
        </tr>
        <tr>
          <td>
            {{ Form::label('', 'Tenaga kependidikan') }}<br/>
            {{ Form::label('', 'Tuliskan data tenaga kependidikan  yang ada di PS, Jurusan, Fakultas
             atau PT yang melayani mahasiswa PS dengan mengikuti format tabel berikut:') }}<br/>
             <button type="button" class="btn btn-info">Button</button><br/>
             <p class="catatan">
               *Hanya yang memiliki pendidikan formal dalam bidang perpustakaan
             </p>
          </td>
        </tr>
        <tr>
          <td>
            {{ Form::label('', 'Jelaskan upaya yang telah dilakukan PS dalam meningkatkan kualifikasi
             dan kompetensi tenaga kependidikan, dalam hal pemberian kesempatan belajar/pelatihan,
             pemberian fasilitas termasuk dana, dan jenjang karir.') }}<br/>
            {{ Form::textarea('tenaga', null, ['class' => 'textarea']) }}
          </td>
        </tr>
				<tr>
					<td colspan="2">
					{{ Form::submit('confirm', ['class' => 'btn btn-info']) }}
					<a href="{{ url('home') }}" class="btn btn-warning">Back</a>
					</td>
				</tr>
				</table>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</body>

</html>
@endsection