<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PraktekFives extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prakteks', function(Blueprint $table){
            $table->increments('id');
            $table->string('nama');
            $table->string('judul');
            $table->time('waktu');
            $table->string('tempat');
            $table->unsignedInteger('standar5_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('prakteks'))
        {
            Schema::drop('prakteks');
        }
    }
}
