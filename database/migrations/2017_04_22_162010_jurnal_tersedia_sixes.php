<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class JurnalTersediaSixes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jurnal_tersedias', function(Blueprint $table){
            $table->increments('id');
            $table->string('jenis');
            $table->string('jurnal');
            $table->string('rincian');
            $table->integer('jumlah');
            $table->unsignedInteger('standar6_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('jurnal_tersedias'))
        {
            Schema::drop('jurnal_tersedias');
        }
    }
}
