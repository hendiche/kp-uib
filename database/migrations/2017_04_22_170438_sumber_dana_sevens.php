<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SumberDanaSevens extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sumber_danas', function(Blueprint $table){
            $table->increments('id');
            $table->string('sumber');
            $table->integer('ts-2');
            $table->integer('ts-1');
            $table->integer('ts');
            $table->unsignedInteger('standar7_id');
            $table->timestamps();           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('sumber_danas'))
        {
            Schema::drop('sumber_danas');
        }
    }
}
