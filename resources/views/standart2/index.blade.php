@extends('layouts.app')

@section('content')
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>{{ config('app.name', 'Laravel') }}</title>

	<!-- Styles -->
	<link href="/css/app.css" rel="stylesheet">

	<!-- Scripts -->
</head>

<body>
	<div class="container">
		<div class="panel panel-default">
			<div class="panel panel-heading">
			<a href="#">
				<span>Standart 2</span>
			</a>
			</div>
			<div class="panel-body">
				{!! Form::open(['url' => '#', 'method' => 'post']) !!}
				<table class="table">
        <tr>
            <td>
              <div> {{ Form::label('', 'Pilih Prodi yang akan di lanjut untuk pengisian isi') }} </div>
              <div>
                <select name="prodi" required>
                  <option value="">Pilih Prodi</option>
                  @foreach($prodi as $list)
                    <option value="{{ $list->id }}">{{ $list->program_studi }} - {{ $list->jurusan }}</option>
                  @endforeach
                </select>
              </div>
            </td>
          </tr>
				<tr>
					<td>
					  <div>
              {{ Form::label('tataPomong', 'Sistem Tata Pomong') }} <br/>
              {{ Form::label('tataPomong', 'Sistem tata pamong berjalan secara efektif melalui mekanisme yang disepakati bersama,
              serta dapat memelihara dan mengakomodasi semua unsur, fungsi, dan peran dalam program studi.
              Tata pamong didukung dengan budaya organisasi yang dicerminkan dengan ada dan tegaknya aturan,
              tatacara pemilihan pimpinan, etika dosen, etika mahasiswa, etika tenaga kependidikan,
              sistem penghargaan dan sanksi serta pedoman dan prosedur pelayanan (administrasi, perpustakaan,
              laboratorium, dan studio). Sistem tata pamong (input, proses, output dan outcome serta lingkungan
              eksternal yang menjamin terlaksananya tata pamong yang baik) harus diformulasikan, disosialisasikan,
              dilaksanakan,  dipantau dan dievaluasi dengan peraturan dan prosedur yang jelas.') }} <br/>
              {{ Form::label('standart2', 'Uraikan secara ringkas sistem dan pelaksanaan tata pamong di Program
              Studi untuk membangun sistem tata pamong yang kredibel, transparan, akuntabel, bertanggung jawab dan
              adil.') }}
            </div>
					<div>{{ Form::textarea('tataPomong', null, ['required' => '', 'class' => 'textarea' ]) }}</div>
				</td>
				</tr>
				<tr>
					<td>
  					<div>
              {{ Form::label('kepemimpinan', 'Kepemimpinan') }} <br/>
              {{ Form::label('kepemimpinan', 'Kepemimpinan efektif mengarahkan dan mempengaruhi perilaku
                semua unsur dalam program studi, mengikuti nilai, norma, etika, dan budaya organisasi yang
                 disepakati bersama, serta mampu membuat keputusan yang tepat dan cepat.
                 Kepemimpinan mampu memprediksi masa depan, merumuskan dan mengartikulasi visi yang realistik,
                 kredibel, serta mengkomunikasikan visi ke depan, yang menekankan pada keharmonisan hubungan
                 manusia dan mampu menstimulasi secara intelektual dan arif bagi anggota untuk mewujudkan visi
                 organisasi, serta mampu memberikan arahan, tujuan, peran, dan tugas kepada seluruh unsur dalam
                 perguruan tinggi. Dalam menjalankan fungsi kepemimpinan dikenal kepemimpinan operasional,
                 kepemimpinan organisasi, dan kepemimpinan publik.  Kepemimpinan operasional berkaitan dengan
                 kemampuan menjabarkan visi, misi ke dalam kegiatan operasional program studi.  Kepemimpinan
                 organisasi berkaitan dengan pemahaman tata kerja antar unit dalam organisasi perguruan tinggi.
                 Kepemimpinan publik berkaitan dengan kemampuan menjalin kerjasama dan menjadi rujukan bagi publik.') }} <br/>
              {{ Form::label('kepemimpinan', 'Jelaskan pola kepemimpinan dalam Program Studi.') }}
            </div>
  					<div>{{ Form::textarea('standart2', null, ['required' => '', 'class' => 'textarea' ]) }}</div>
					</td>
				</tr>
				<tr>
					<td>
					<div>{{ Form::label('pengelolaan', 'Sistem Pengelolaan') }} <br/>
            {{ Form::label('pengelolaan' ,'Sistem pengelolaan fungsional dan operasional program studi mencakup planning,
            organizing, staffing, leading, controlling dalam kegiatan  internal maupun eksternal.') }} <br/>
            {{ Form::label('pengelolaan', 'Jelaskan sistem pengelolaan Program Studi serta dokumen pendukungnya.') }}
            </div>
					<div>{{ Form::textarea('standart2', null, ['required' => '', 'class' => 'textarea' ]) }}</div>
					</td>
				</tr>
				<tr>
					<td>
					<div>
            {{ Form::label('jaminMutu', 'Penjamin Mutu') }} <br/>
            {{ Form::label('jaminMutu', 'Bagaimanakah pelaksanaan penjamin mutu pada Program Studi? Jelaskan.') }}
            </div>
					<div>{{ Form::textarea('standart2', null, ['required' => '', 'class' => 'textarea' ]) }}</div>
					</td>
				</tr>
				<tr>
					<td>
					 <div>
            {{ Form::label('umpanBalik', 'Umpan Balik') }}
            {{ Form::label('umpanBalik', 'Apakah program studi telah melakukan kajian tentang proses
             pembelajaran melalui umpan balik dari dosen, mahasiswa, alumni, dan pengguna lulusan
              mengenai harapan dan persepsi mereka?  Jika Ya, jelaskan isi umpan balik dan tindak
               lanjutnya.') }}
           </div>
					</td>
				</tr>

				<tr>
					<td>
  					<div>
              {{ Form::label('keberlanjtan', 'Keberlanjutan') }}
              {{ Form::label('keberlanjtan', 'Jelaskan upaya untuk menjamin keberlanjutan (sustainability) program studi ini, khususnya dalam hal:') }}
              </div>
  					<div> {{ Form::label('calon', 'a. Upaya untuk peningkatan animo calon mahasiswa:') }}</div>
  					<div> {{ Form::textarea('calon', null, ['required' => '', 'size' => '80x3']) }}</div>

  					<div> {{ Form::label('manajemen', 'b. Upaya peningkatan mutu manajemen:') }}</div>
  					<div> {{ Form::textarea('manajemen', null, ['required' => '', 'size' => '80x3']) }}</div>

  					<div> {{ Form::label('lulusan', 'c. Upaya untuk peningkatan mutu lulusan:') }}</div>
  					<div> {{ Form::textarea('lulusan', null, ['required' => '', 'size' => '80x3']) }}</div>

  					<div> {{ Form::label('kemitraan', 'd. Upaya untuk pelaksanaan dan hasil kerjasama kemitraan:') }}</div>
  					<div> {{ Form::textarea('kemitraan', null, ['required' => '', 'size' => '80x3']) }}</div>

  					<div> {{ Form::label('hibah', 'e. Upaya dan prestasi memperoleh dana hibah kompetitif:') }}</div>
  					<div> {{ Form::textarea('hibah', null, ['required' => '', 'size' => '80x3']) }}</div>
					</td>
				</tr>

				<tr>
					<td colspan="2">
					{{ Form::submit('confirm', ['class' => 'btn btn-info']) }}
					<a href="{{ url('home') }}" class="btn btn-warning">Back</a>
					</td>
				</tr>
				</table>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</body>

</html>
@endsection