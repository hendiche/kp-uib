<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PrestasiThrees extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prestasi_tigas', function(Blueprint $table) {
          $table->increments('id');
          $table->string('nama_kegiatan');
          $table->string('tingkat');
          $table->string('hasil_prestasi');
          $table->unsignedInteger('standar3_id');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('prestasi_tigas'))
        {
          Schema::drop('prestasi_tigas');
        }
    }
}
