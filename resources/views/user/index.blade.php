@extends('layouts.app')

@section('content')
<!DOCTYPE html>
<html>
<head>
  <title></title>
</head>
<body>
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-2 col-sm-2 noPadding">
        @include('layouts.navigation')
      </div>
      <div class="col-md-10 col-sm-10 noPadding">
        <div class="panel panel-default">
          <div class="panel-heading">
            <p>Create New User</p>
          </div>
          <div class="panel-body">
            <table class="table" id="myTable">
              <thead>
                <tr role="row">
                  <td>Name</td>
                  <td>Email</td>
                  <td>NIDN</td>
                  <td>Jabatan</td>
                  <td>Role</td>
                  <td>Option</td>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal">Add User</button>
            <div class="modal fade" id="myModal" role="dialog">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button class="close" type="button" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add New User</h4>
                  </div>
                  <div class="modal-body">
                    {!! Form::open(['url' => 'user/add', 'method' => 'post']) !!}
                      <table class="table">
                        <tr>
                          <td> {{ Form::label('', 'Name') }} </td>
                          <td> {{ Form::text('name', null, ['required' => '']) }} </td>
                        </tr>
                        <tr>
                          <td> {{ Form::label('', 'Email') }} </td>
                          <td> {{ form::email('email', null, ['required' => '']) }} </td>
                        </tr>
                        <tr>
                          <td> {{ Form::label('', 'Password') }} </td>
                          <td> {{ Form::password('password', ['required' => '', 'minlength' => '4']) }} </td>
                        </tr>
                        <tr>
                          <td> {{ Form::label('', 'NIDN') }} </td>
                          <td> {{ Form::text('nidn', null) }} </td>
                        </tr>
                        <tr>
                          <td> {{ Form::label('', 'Jabatan') }} </td>
                          <td> {{ Form::text('jabatan', null) }} </td>
                        </tr>
                        <tr>
                          <td> {{ Form::label('', 'Role') }} </td>
                          <td> {{ Form::select('role', [
                              'admin' => 'Admin',
                              'prodi' => 'Prodi',
                              'assesor' => 'Assesor'
                              ]) }}
                          </td>
                        </tr>
                      </table>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    {{ Form::submit('confirm', ['class' => 'btn btn-primary']) }}
                  {!! Form::close() !!}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</body>
</html>
@endsection
@section('script')
<script type="text/javascript">
  $(document).ready(function() {
    var DataTable = $('#myTable').DataTable(
    {
      dom: 'lrtip',
      responsive: true,
      processing: true,
      serverSide: true,
      ajax: {
        url: '{{ route('item.datatable') }}',
        data: function(data) {
          data._token = '{{ csrf_token() }}';
        },
        type: 'POST',
      },
      columns: [
        { data: 'name', name: 'name' },
        { data: 'email', name: 'email' },
        { data: 'nidn', name: 'nidn' },
        { data: 'jabatan', name: 'jabatan' },
        { data: 'peran', name: 'peran' },
        { data: 'action', name: 'action', orderable: false, searchable: false }
      ]
    });
  })
  // $(document).ready(function(){
  //   $('#myTable').DataTable();
  // });
</script>
@endsection