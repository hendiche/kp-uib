@extends('layouts.app')

@section('content')
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>{{ config('app.name', 'Laravel') }}</title>

	<!-- Styles -->
	<link href="/css/app.css" rel="stylesheet">

	<!-- Scripts -->
</head>

<body>
	<div class="container">
		<div class="panel panel-default">
			<div class="panel panel-heading">
			<a href="#">
				<span>Standart 6</span>
			</a>
			</div>
			<div class="panel-body">
				{!! Form::open(['url' => '#', 'method' => 'post']) !!}
				<table class="table">
					<tr>
						<td>
						<div>
						{{ Form::label('', 'Pengelolaan Dana') }}<br/>
						{{ Form::label('', 'Keterlibatan aktif program studi harus tercerminkan dalam dokumen tentang proses perencanaan, pengelolaan dan pelaporan serta pertanggungjawaban penggunaan dana kepada pemangku kepentingan melalui mekanisme yang transparan dan akuntabel.') }}<br/>
						{{ Form::label('', 'Jelaskan keterlibatan PS dalam perencanaan anggaran dan pengelolaan dana.') }}
						<div> {{ Form::textarea('evaluasi', null, ['required' => '', 'class' => 'textarea' ]) }}</div>
						</div>
						</td>
					</tr>
					<tr>
						<td>
						<div>
						{{ Form::label('', 'Perolehan dan Alokasi Dana') }}<br/>
						{{ Form::label('', 'Tuliskan realisasi perolehan dan alokasi dana (termasuk hibah) dalam juta rupiah termasuk gaji, selama tiga tahun terakhir, pada tabel berikut:') }}<br/>
						<button type="button" class="btn btn-info">Button</button><br/>
						{{ Form::label('', 'Penggunaan Dana:') }}<br/>
						<button type="button" class="btn btn-info">Button</button>
						</div>
						</td>
					</tr>
					<tr>
						<td>
						<div>
							{{ Form::label('', 'Tuliskan dana untuk kegiatan penelitian pada tiga tahun terakhir yang melibatkan dosen yang bidang keahliannya sesuai dengan program studi, dengan mengikuti format tabel berikut:') }}<br/>
							<button type="button" class="btn btn-info">Button</button><br/>
							<p class="catatan">
							* Di luar dana penelitian/penulisan skripsi, tesis, dan disertasi sebagai bagian dari studi lanjut.
							</p>
						</div>
						</td>
					</tr>
					<tr>
						<td>
						<div>
							{{ Form::label('', 'Tuliskan dana yang diperoleh dari/untuk kegiatan pelayanan/pengabdian kepada masyarakat pada tiga tahun terakhir dengan mengikuti format tabel berikut:') }}<br/>
							<button type="button" class="btn btn-info">Button</button>
							</div>
						</td>
					</tr>
					<tr>
						<td>
						<div>
							{{ Form::label('', 'Prasarana') }}<br/>
							{{ Form::label('', 'Tuliskan data ruang kerja dosen tetap yang bidang keahliannya sesuai dengan PS dengan mengikuti format tabel berikut:') }}
							<button type="button" class="btn btn-info">Button</button>
						</div>
						</td>
					</tr>
					<tr>
						<td>
						<div>
							{{ Form::label('', 'Tuliskan data prasarana (kantor, ruang kelas, ruang laboratorium, studio, ruang perpustakaan, kebun percobaan, dsb. kecuali  ruang dosen) yang dipergunakan PS dalam proses belajar mengajar dengan  mengikuti format tabel berikut:') }}<br/>
							<button type="button" class="btn btn-info">Button</button><br/>
							<p class="catatan">
								Keterangan: <br/>
								SD = Milik PT/fakultas/jurusan sendiri;<br/>
								SW = Sewa/Kontrak/Kerjasama
							</p>
						</div>
						</td>
					</tr>
					<tr>
						<td>
						<div>
							{{ Form::label('', 'Tuliskan data prasarana lain yang menunjang (misalnya tempat olah raga, ruang bersama, ruang himpunan mahasiswa, poliklinik) dengan mengikuti format tabel berikut:') }}<br/>
							<button type="button" class="btn btn-info">Button</button><br/>
							<p class="catatan">
								Keterangan: <br/>
								SD = Milik PT/fakultas/jurusan sendiri;<br/>
								SW = Sewa/Kontrak/Kerjasama
							</p>
						</div>
						</td>
					</tr>
					<tr>
						<td>
						<div>
							{{ Form::label('', 'Sarana Pelaksanaan Kegiatan Akademik') }}<br/>
							{{ Form::label('', 'Pustaka (buku teks, karya ilmiah, dan jurnal; termasuk juga dalam bentuk CD-ROM dan media lainnya). Tuliskan rekapitulasi jumlah ketersediaan pustaka yang relevan dengan bidang PS dengan mengikuti format tabel 1 berikut: ') }}
							<button type="button" class="btn btn-info">Button</button><br/>
							{{ Form::label('', 'Isikan jurnal/prosiding seminar yang tersedia/yang diterima secara teratur (lengkap), terbitan 3 tahun terakhir dengan mengikuti format tabel 2 berikut:') }}<br/>
							<button type="button" class="btn btn-info">Button</button>
							<p class="catatan">
							Catatan *=termasuk e-journal
							</p>
						</div>
						</td>
					</tr>
					<tr>
						<td>
						<div>
							{{ Form::label('', 'Sebutkan sumber-sumber pustaka di lembaga lain (lembaga perpustakaan/ sumber dari internet beserta  alamat website) yang biasa diakses/dimanfaatkan oleh dosen dan mahasiswa program studi ini.') }}<br/>
							<div> {{ Form::textarea('', null, ['required' => '', 'class' => 'textarea' ]) }}</div>
						</div>
						</td>
					</tr>
					<tr>
						<td>
						<div>
						{{ Form::label('', 'Tuliskan peralatan utama yang digunakan di laboratorium (tempat praktikum, bengkel, studio, ruang simulasi, rumah sakit, puskesmas/balai kesehatan, green house, lahan untuk pertanian, dan sejenisnya) yang dipergunakan dalam proses pembelajaran di jurusan/fakultas dengan  mengikuti format tabel berikut:') }}<br/>
						<button type="button" class="btn btn-info">Button</button><br/>
						<p class="catatan">
						Keterangan: <br/>
						SD = Milik PT/fakultas/jurusan sendiri;<br/>
						SW = Sewa/Kontrak/Kerjasama/Hak Pakai
						</p>
						</div>
						</td>
					</tr>
					<tr>
						<td>
						<div>
						{{ Form::label('', 'Sistem Informasi') }}<br/>
						{{ Form::label('', 'Jelaskan sistem informasi dan fasilitas yang digunakan oleh program studi untuk proses pembelajaran (hardware, software, e-learning, perpustakaan, dll.).') }}<br/>
						<div> {{ Form::textarea('', null, ['required' => '', 'class' => 'textarea' ]) }}</div>
						</div>
						</td>
					</tr>
					<tr>
						<td>
						<div>
						{{ Form::label('', 'Beri tanda √ pada kolom yang sesuai (hanya satu kolom) dengan aksesibilitas tiap jenis data, dengan mengikuti format tabel berikut:') }}<br/>
						<button type="button" class="btn btn-info">Button</button>
						</div>
						</td>
					</tr>

					<tr>
						<td colspan="2">
						{{ Form::submit('confirm', ['class' => 'btn btn-info']) }}
						<a href="{{ url('home') }}" class="btn btn-warning">Back</a>
						</td>
					</tr>

				</table>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</body>

</html>
@endsection