<?php

namespace App\Http\Controllers;

use App\Http\Controllers\MasterController;
use Illuminate\Http\Request;

class standart5controller extends Controller
{
    public function __construct() {
    	$this->middleware('auth');
    }

    public function index() {
    	$matkul = "test";
    	return view('standart5/index')->with([
    		'matkul' => $matkul
    	]);
    }

    public function store(Request $request){
    	
    	//test pemanggilan data
    	dd(
    		$request->semester,
    		$request->kodeMK,
    		$request->namaMK,
    		$request->bobotSKS,
    		$request->bobotTugas,
    		$request->unit,
    		$request->total_BobotSKS
    	);
    	//jadi tinggal assign nilai ini ke dalam database, tidak ada model; jadi saya sampai sini saja


    	return view('/');
    }
}
