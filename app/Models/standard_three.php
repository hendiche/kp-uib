<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;

class standard_three extends Authenticatable
{
    protected $fillable = [
		'evaluasi_lulusan', 'kerja_pertama', 'kerja_sesuai_bidang', 'himpunan_alumni', 'jumlah',
    ];

    protected $guarded = [
    	'id', 'prodi_id',
    ];

    protected $hidden = [
		
    ];

    public function prodi()
	{
		return $this->belongsTo('App\Models\prody_identities');
	}
}
