<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PrasaranaSixes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prasaranas', function(Blueprint $table){
            $table->increments('id');
            $table->string('jenis');
            $table->integer('jumlah');
            $table->integer('luas');
            $table->string('sd');
            $table->string('sw');
            $table->string('terawat');
            $table->string('tdk_terawat');
            $table->dateTime('utilisasi');
            $table->unsignedInteger('standar6_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('prasaranas'))
        {
            Schema::drop('prasaranas');
        }
    }
}
