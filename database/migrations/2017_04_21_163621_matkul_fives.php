<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MatkulFives extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matkuls', function(Blueprint $table){
            $table->increments('id');
            $table->string('smt');
            $table->string('kode');
            $table->string('nama_matkul');
            $table->integer('bobot_sks');
            $table->decimal('inti', 3, 0);
            $table->decimal('institusional', 3, 0);
            $table->integer('bobot_tugas');
            $table->decimal('deskripsi', 3, 0);
            $table->decimal('silabus', 3, 0);
            $table->decimal('sap', 3, 0);
            $table->string('unit');
            $table->unsignedInteger('standar5_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('matkuls'))
        {
            Schema::drop('matkuls');
        }
    }
}
