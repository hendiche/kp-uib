<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TenagaPembicaraFours extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tenaga_pembicara_empats', function(Blueprint $table) {
          $table->increments('id');
          $table->string('nama');
          $table->string('judul');
          $table->dateTime('waktu');
          $table->unsignedInteger('standar4_id');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('tenaga_pembicara_empats'))
        {
          Schema::drop('tenaga_pembicara_empats');
        }
    }
}
