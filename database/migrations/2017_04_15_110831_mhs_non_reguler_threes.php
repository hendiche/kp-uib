<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MhsNonRegulerThrees extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mhs_non_reguler_tigas', function(Blueprint $table) {
          $table->increments('id');
          $table->string('thn_akademik');
          $table->integer('calon_mhs_ikut');
          $table->integer('calon_mhs_lulus');
          $table->integer('mhs_baru_no_trans');
          $table->integer('mhs_baru_trans');
          $table->integer('total_mhs_no_trans');
          $table->integer('total_mhs_trans');
          $table->unsignedInteger('standar3_id');
          $table->timestamps();
        });
    }

    /**
     * reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('mhs_non_reguler_tigas'))
        {
          Schema::drop('mhs_non_reguler_tigas');
        }
    }
}
