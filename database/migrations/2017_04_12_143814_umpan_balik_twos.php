<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UmpanBalikTwos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('umpan_balik_duas', function(Blueprint $table) {
          $table->increments('id');
          $table->string('umpan_dari');
          $table->string('umpan_isi');
          $table->string('tindak_lanjut');
          $table->unsignedInteger('standar2_id');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('umpan_balik_duas'))
        {
          Schema::drop('umpan_balik_duas');
        }
    }
}
