<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RuangDosenSixes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ruang_dosens', function(Blueprint $table){
            $table->increments('id');
            $table->string('ruang');
            $table->integer('jumlah');
            $table->integer('luas');
            $table->unsignedInteger('standar6_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('ruang_dosens'))
        {
            Schema::drop('ruang_dosens');
        }
    }
}
