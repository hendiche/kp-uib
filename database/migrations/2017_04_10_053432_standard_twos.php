<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StandardTwos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*Standar 2. Tata Pamong, Kepemimpinan, Sistem Pengelolaan, dan Penjamin Mutu*/

        Schema::create('standar_duas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tata_pamong')->nullable();
            $table->string('kepemimpinan')->nullable();
            $table->string('pengelolaan')->nullable();
            $table->string('penjamin_mutu')->nullable();
            //$table->unsignedInteger('umpan_balik')->nullable();
            $table->string('calon_mhs')->nullable();
            $table->string('mutu_manajemen')->nullable();
            $table->string('mutu_lulusan')->nullable();
            $table->string('kemitraan')->nullable();
            $table->string('hibah')->nullable();
            $table->integer('jumlah')->nullable();
            $table->unsignedInteger('prodi_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('standar_duas'))
        {
            Schema::drop('standar_duas');
        }

    }
}
