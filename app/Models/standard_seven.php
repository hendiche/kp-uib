<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;

class standard_seven extends Authenticatable
{
    protected $fillable = [
        'mhs_penelitian', 'mhs_skripsi', 'judul_ilmiah', 'karya_hak', 'tingkat_partisipasi', 'sum',
    ];

    protected $guarded = [
    	'prodi_id',
    ];

    protected $hidden = [
        'id',
    ];

    public function prodi()
  	{
    	return $this->belongsTo('App\Models\prody_identities');
  	}
}
