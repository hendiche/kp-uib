<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AktivitasNgajarDosenFours extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aktivitas_ngajar_dosen_empats', function (Blueprint $table) {
          $table->increments('id');
          $table->unsignedInteger('aktivitas_dosen_id');
          $table->unsignedInteger('standar4_id');
          $table->enum('tipe', ['sesuai', 'tdk_sesuai']);
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('aktivitas_ngajar_dosen_empats'))
        {
          Schema::drop('aktivitas_ngajar_dosen_empats');
        }
    }
}
