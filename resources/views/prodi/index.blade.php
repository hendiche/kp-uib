@extends('layouts.app')

@section('content')
<!DOCTYPE html>
<html>
<head>
  <title></title>
</head>
<body>
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-2 col-sm-2 noPadding">
        @include('layouts.navigation')
      </div>
      <div class="col-md-10 col-sm-10 noPadding">
        <div class="panel panel-default">
          <div class="panel-heading">
            <a href="#">
              <span>Idetitas Program Studi</span>
            </a>
          </div>
          <div class="panel-body">
            {!! Form::open(['url' => 'prodi/insert', 'method' => 'post']) !!}
            <table class="table">
              <tr>
                <td> {{ Form::label('prodi', 'Program Studi (PS)') }} </td>
                <td> {{ Form::text('prodi', null, ['required' => '', 'class' => 'sizeBox']) }} </td>
              </tr>
              <tr>
                <td> {{ Form::label('jurusan', 'Jurusan/Departemen') }} </td>
                <td> {{ Form::text('jurusan', null, ['required' => '', 'class' => 'sizeBox']) }} </td>
              </tr>
              <tr>
                <td>{{ Form::label('fakultas', 'Fakultas') }}</td>
                <td>{{ Form::text('fakultas', null, ['required' => '', 'class' => 'sizeBox']) }}</td>
              </tr>
              <tr>
                <td>{{ Form::label('perguruTinggi', 'Perguruan Tinggi') }}</td>
                <td>{{ Form::text('perguruTinggi', null, ['required' => '', 'class' => 'sizeBox']) }}</td>
              </tr>
              <tr>
                <td>{{ Form::label('noSK', 'No. SK Pendirian PS') }}</td>
                <td>{{ Form::text('noSK', null, ['required' => '', 'class' => 'sizeBox']) }}</td>
              </tr>
              <tr>
                <td>{{ Form::label('tglSK', 'Tanggal SK Pendirian PS') }}</td>
                <td>{{ Form::text('tglSK', null, ['id' => 'pendirian', 'class' => 'sizeBox', 'readonly']) }}</td>
                {{-- <td>{{ Form::date('tglSK', \Carbon\carbon::now()), ['class' => 'sizeBox'] }}</td> --}}
              </tr>
              <tr>
                <td>{{ Form::label('pejabatSK', 'Pejabat Penandatangan SK Pendirian PS') }}</td>
                <td>{{ Form::text('pejabatSK', null, ['required' => '', 'class' => 'sizeBox']) }}</td>
              </tr>
              <tr>
                <td>{{ Form::label('mulaiPS', 'Bulan & Tahun Dimulainya Penyelenggaraan PS') }}</td>
                <td>{{ Form::text('mulaiPS', null, ['id' => 'penyelenggaraan', 'class' => 'sizeBox', 'readonly']) }}</td>
                {{-- <td>{{ Form::date('mulaiPS', \Carbon\carbon::now()) }}</td> --}}
              </tr>
              <tr>
                <td>{{ Form::label('noSKizin', 'No. SK Izin Operasional') }}</td>
                <td>{{ Form::text('noSKizin', null, ['required' => '', 'class' => 'sizeBox']) }}</td>
              </tr>
              <tr>
                <td>{{ Form::label('tglSKizin', 'Tanggal SK Izin Operasional') }}</td>
                <td>{{ Form::text('tglSKizin', null, ['id' => 'izin', 'class' => 'sizeBox', 'readonly']) }}</td>
                {{-- <td>{{ Form::date('tglSKizin', \Carbon\carbon::now()) }}</td> --}}
              </tr>
              <tr>
                <td>{{ Form::label('peringkat', 'Peringat (Nilai) Akreditasi Terakhir') }}</td>
                <td>{{ Form::text('peringkat', null, ['required' => '', 'class' => 'sizeBox']) }}</td>
              </tr>
              <tr>
                <td>{{ Form::label('noSKBANPT', 'No. SK BAN-PT') }}</td>
                <td>{{ Form::text('noSKBANPT', null, ['required' => '', 'class' => 'sizeBox']) }}</td>
              </tr>
              <tr>
                <td>{{ Form::label('alamat', 'Alamat PS') }}</td>
                <td>{{ Form::textarea('alamat', null, ['class' => 'formTextArea']) }}</td>
              </tr>
              <tr>
                <td>{{ Form::label('noTelpPS', 'No. Telepon PS') }}</td>
                <td>{{ Form::text('noTelpPS', null, ['required' => '', 'class' => 'sizeBox']) }}</td>
              </tr>
              <tr>
                <td>{{ Form::label('noFaksimili', 'No. Faksimili PS') }}</td>
                <td>{{ Form::text('noFaksimili', null, ['required' => '', 'class' => 'sizeBox']) }}</td>
              </tr>
              <tr>
                <td>{{ Form::label('emailPS', 'Homepage dan E-mail PS') }}</td>
                <td>{{ Form::text('emailPS', null, ['required' => '', 'class' => 'sizeBox']) }}</td>
              </tr>
              <tr>
                <td colspan="2">
                  <p class="bold">Bagi PS yang dibina oleh Departemen Pendidikan Nasional, sebutkan nama dosen tetap institusi yang terdaftar sebagai dosen tetap PS berdasarkan SK 034/DIKTI/Kep/2002, dalam tabel</p>
                  <div>
                    <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal">Add Dosen</button>
                  </div>
                </td>
              </tr>
              <tr>
                <td colspan="2">
                {{ Form::submit('confirm', ['class' => 'btn btn-primary toRight']) }}
                </td>
              </tr>
            </table>
            {!! Form::close() !!}
            {{-- ######################################################################################## --}}
            <div class="modal fade" id="myModal" role="dialog">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Dosen</h4>
                  </div>
                  <div class="modal-body">
                    <p>asd</p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">cancel</button>
                    <button type="button" class="btn btn-default" data-toggle="modal" data-target="#testModal">footer button</button>
                  </div>
                </div>
              </div>
            </div>
            {{-- ############################################################ --}}
          </div> {{-- CLOSE PANEL BODY--}}
        </div>
      </div>
    </div>
  </div>
</body>
</html>
@endsection
@section('script')
<script type="text/javascript">
  $(function() {
    $("#pendirian" ).datepicker({
      changeMonth: true,
      changeYear: true,
      dateFormat: 'dd-MM-yy'
    });
    $('#penyelenggaraan').datepicker({
      changeYear: true,
      changeMonth: true,
      dateFormat: 'MM yy'
    });
    $('#izin').datepicker({
      changeMonth: true,
      changeYear: true,
      dateFormat: 'dd-MM-yy'
    });
  });
</script>
@endsection