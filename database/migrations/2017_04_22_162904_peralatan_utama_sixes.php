<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PeralatanUtamaSixes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('peralatan_utamas', function(Blueprint $table){
            $table->increments('id');
            $table->string('nama');
            $table->string('jenis_peralatan');
            $table->integer('jml_unit');
            $table->string('sd');
            $table->string('sw');
            $table->string('terawat');
            $table->string('tdk_terawat');
            $table->dateTime('rata_rata');
            $table->unsignedInteger('standar6_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('peralatan_utamas'))
        {
            Schema::drop('peralatan_utamas');
        }
    }
}
