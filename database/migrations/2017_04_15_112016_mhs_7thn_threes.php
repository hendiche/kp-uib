<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Mhs7thnThrees extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mhs_7thn_tigas', function(Blueprint $table) {
          $table->increments('id');
          $table->string('thn_masuk');
          $table->integer('ts-6');
          $table->integer('ts-5');
          $table->integer('ts-4');
          $table->integer('ts-3');
          $table->integer('ts-2');
          $table->integer('ts-1');
          $table->integer('ts');
          $table->integer('jumlah_lulusan');
          $table->unsignedInteger('standar3_id');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('mhs_7thn_tigas'))
        {
          Schema::drop('mhs_7thn_tigas');
        }
    }
}
