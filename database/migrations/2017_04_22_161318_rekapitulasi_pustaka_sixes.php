<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RekapitulasiPustakaSixes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rekapitulasi_pustakas', function(Blueprint $table){
            $table->increments('id');
            $table->string('jenis');
            $table->integer('jml_judul');
            $table->integer('jml_copy');
            $table->unsignedInteger('standar6_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('rekapitulasi_pustakas'))
        {
            Schema::drop('rekapitulasi_pustakas');
        }
    }
}
