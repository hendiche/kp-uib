<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StandardOnes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*Standar 1. Visi, Misi, Tujuan dan Sasaran, Serta Strategi Pencapaian*/

        Schema::create('standar_satus', function (Blueprint $table) {
            $table->increments('id');
            $table->string('mekanis_susun')->nullable();
            $table->string('visi')->nullable();
            $table->string('misi')->nullable();
            $table->string('tujuan')->nullable();
            $table->string('sasaran_strategi')->nullable();
            $table->string('sosialisasi')->nullable();
            $table->integer('jumlah')->nullable();
            $table->unsignedInteger('prodi_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('standar_satus'))
        {
            Schema::drop('standar_satus');
        }
    }
}
