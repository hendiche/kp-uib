<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;

class standard_two extends Authenticatable
{
    protected $fillable = [
        'tata_pamong,', 'kepemimpinan', 'pengelolaan', 'penjamin_mutu', 'umpan_balik', 'calon_mhs', 'mutu_manajemen', 'kemitraan', 'hibah', 'jumlah',
    ];

    protected $guarded = [
    	'id', 'prodi_id',
    ];

    protected $hidden = [
        
    ];

    public function prodi()
    {
    	return $this->belongsTo('App\Models\prody_identities');
    }

    public function umpan_balik_two()
    {
      return $this->hasOne('App\Models\umpan_balik_2');
    }
}
