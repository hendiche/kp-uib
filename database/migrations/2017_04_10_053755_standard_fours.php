<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StandardFours extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('standar_empats', function (Blueprint $table) {
            $table->increments('id');
            $table->string('seleksi_pengembangan')->nullable();
            $table->string('monitor_evaluasi')->nullable();
            // $table->unsignedInteger('aktivitas_dosen')->nullable();
            // $table->unsignedInteger('dosen_nontetap')->nullable();
            // $table->unsignedInteger('pembicara')->nullable();
            // $table->unsignedInteger('dosen_tugas_belajar')->nullable();
            // $table->unsignedInteger('dosen_seminar')->nullable();
            // $table->unsignedInteger('prestasi_dosen')->nullable();
            // $table->unsignedInteger('dosen_organisasi')->nullable();
            // $table->unsignedInteger('tenaga_kependidikan')->nullable();
            $table->string('prodi_kualifikasi')->nullable();
            $table->integer('jumlah')->nullable();
            $table->unsignedInteger('prodi_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('standar_empats'))
        {
            Schema::drop('standar_empats');
        }
    }
}
