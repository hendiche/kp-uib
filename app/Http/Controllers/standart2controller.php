<?php

namespace App\Http\Controllers;

use App\Http\Controllers\MasterController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\prody_identities;
use App\Models\standard_two;

class standart2controller extends Controller
{
    public function __construct()
    {
    	$this->middleware('auth');
    }

    public function index()
    {
      $prodi = new prody_identities();
    	return view('standart2/index')->with('prodi', $prodi->get());
    }
}
