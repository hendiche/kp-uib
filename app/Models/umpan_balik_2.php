<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;

class umpan_balik_2 extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'umpan_dari', 'umpan_isi', 'tindak_lanjut'
    ];

    protected $guarded = [
        'id'
    ];

    protected $hidden = [
        
    ];

    protected $dates = [
        'updated_at', 'created_at'
    ];

    public function standardTwo()
    {
      return $this->belongsTo('App\Models\Standard_two');
    }
}
