<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MatkulPilihanFives extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matkul_pilihans', function(Blueprint $table){
            $table->increments('id');
            $table->string('semester');
            $table->string('kode_matkul');
            $table->string('nama_matkul');
            $table->integer('bobot_sks');
            $table->integer('bobot_tugas');
            $table->string('unit');
            $table->unsignedInteger('standar5_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('matkul_pilihans'))
        {
          Schema::drop('matkul_pilihans');
        }
    }
}
