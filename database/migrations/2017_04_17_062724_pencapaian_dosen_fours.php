<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PencapaianDosenFours extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pencapaian_dosen_empats', function(Blueprint $table) {
          $table->increments('id');
          $table->string('nama');
          $table->string('prestasi');
          $table->time('waktu');
          $table->string('tingkat');
          $table->unsignedInteger('standar4_id');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('pencapaian_dosen_empats'))
        {
          Schema::drop('pencapaian_dosen_empats');
        }
    }
}
