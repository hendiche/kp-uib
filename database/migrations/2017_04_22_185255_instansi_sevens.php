<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InstansiSevens extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('instansis', function(Blueprint $table){
            $table->increments('id');
            $table->string('nama');
            $table->string('jenis');
            $table->time('mulai');
            $table->time('akhir');
            $table->string('manfaat');
            $table->enum('kegiatan', ['dalam', 'luar']);
            $table->unsignedInteger('standar7_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('instansis'))
        {
            Schema::drop('instansis');
        }
    }
}
