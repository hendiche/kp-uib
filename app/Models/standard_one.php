<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;

class standard_one extends Authenticatable
{
  protected $fillable = [
    'mekanis_susun', 'visi', 'misi', 'tujuan', 'sasaran_strategi', 'sosialisasi', 'sum',
  ];

  protected $guarded = [
    'prodi_id',
  ];

  protected $hidden = [
    'id',
  ];

  public function prodi()
  {
    return $this->belongsTo('App\Models\prody_identities');
  }
}
