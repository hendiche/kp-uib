<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;

class dosen extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
      'no', 'nama', 'nidn', 'tgl_lahir', 'jabatan', 'gelar', 'pendidikan', 'bidang_keahlian', 'tipe_dosen'
    ];

    protected $guarded = [
      ''
    ];

    protected $hidden = [
      'id'
    ];

    public function prody_identities()
    {
      return $this->belongsToMany('App\Models\prody_identities')->withPivot('type');
    }

    public function standardFour()
    {
      return $this->belongsToMany('App\Models\standard_four')->withPivot('type');
    }
}
