<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\dosen;
use App\Http\Controllers\masterController;

class dosenController extends masterController
{
    protected $attributes = [
      'title' => 'Control Panel - Dosen',
    ];

    protected $index_view = 'dosen.index';
    protected $form_view = 'dosen.form';
    protected $redirect_when_success = 'dosen.index';

    public function dataTable($model = null)
    {
      $builder = $this->prepareDataTable(dosen::class);
      return $this->dataTableMaker($builder);
    }

    public function dataTableMaker($builder)
    {
      return $builder
        ->addColumn('action', function ($model) {
            return '<a class="btn btn-info" href='.route('dosen.edit', ['dosen' => $model]).'>Edit</a>';
        })
        ->make(true);
    }

    public function create()
    {
      $this->attributes['route'] = route('dosen.store');
      $this->attributes['method'] = 'post';
      return $this->render(view($this->form_view));
    }

    public function store(Request $req, dosen $dosen = null)
    {
      if (!$dosen) {
        $dosen = new dosen();
      }

      $count = dosen::count();
      $dosen->no = $dosen->no ? $dosen->no : $count + 1;
      $dosen->nama = $req->nama;
      $dosen->nidn = $req->nidn;
      $dosen->tgl_lahir = \Carbon\carbon::parse($req->tgl_lahir)->format('Y-m-d');
      $dosen->jabatan = $req->jabatan;
      $dosen->gelar = $req->gelar;
      $dosen->pendidikan = $req->pendidikan;
      $dosen->bidang_keahlian = $req->bidang_keahlian;
      $dosen->tipe_dosen = $req->tipe_dosen;
      $dosen->save();

      return redirect()->route($this->redirect_when_success);
    }

}
