<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StandardSixes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('standar_enams', function (Blueprint $table) {
            $table->increments('id');
            $table->string('pengelolaan_data')->nullable();
            //$table->unsignedInteger('alokasi_dana')->nullable();
            //$table->unsignedInteger('penggunaan_dana')->nullable();
            //$table->unsignedInteger('dana_penelitian');
            //$table->unsignedInteger('dana_kegiatan');
            //$table->unsignedInteger('ruang_dosen')->nullable();
            //$table->unsignedInteger('prasarana');
            //$table->unsignedInteger('prasarana_penunjang');
            //$table->unsignedInteger('rekapitulasi_pustaka')->nullable();
            //$table->unsignedInteger('jurnal_tersedia')->nullable();
            $table->string('sumber_pustaka')->nullable();
            //$table->unsignedInteger('peralatan_utama')->nullable();
            $table->string('fasilitas_prodi')->nullable();
            //$table->unsignedInteger('aksesibilitas')->nullable();
            $table->integer('jumlah')->nullable();
            $table->unsignedInteger('prodi_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('standar_enams'))
        {
            Schema::drop('standar_enams');
        }
    }
}
