<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class KeikutsertaanFours extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('keikutsertaan_empats', function(Blueprint $table) {
          $table->increments('id');
          $table->string('nama');
          $table->string('organisasi');
          $table->time('waktu');
          $table->string('tingkat');
          $table->unsignedInteger('standar4_id');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('keikutsertaan_empats'))
        {
          Schema::drop('keikutsertaan_empats');
        }
    }
}
