<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StandardFives extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('standar_limas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kompetensi_utama')->nullable();
            $table->string('kompetensi_pendukung')->nullable();
            $table->string('kompetensi_lainnya')->nullable();
            $table->integer('jumlah_sks')->nullable();
            $table->string('mekanisme_kurikulum')->nullable(); // 5.2
            $table->string('mekanisme_matkul')->nullable(); // 5.3
            $table->string('avg_skripsi')->nullable(); // 5.1.1
            $table->string('avg_jml_pertemuan')->nullable(); // 5.1.1
            $table->string('sosialisasi_pelaksanaan')->nullable(); // 5.1.1 (boolean)
            $table->string('avg_skripsi_bulan')->nullable(); // 5.5.2
            $table->string('suasana_akademik')->nullable();
            $table->string('ketersediaan_akademik')->nullable();
            $table->string('akademik_kondusif')->nullable();
            $table->string('interaksi_akademik')->nullable();
            $table->string('pengembangan_perilaku')->nullable();
            $table->integer('jumlah')->nullable();
            $table->unsignedInteger('prodi_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('standar_limas'))
        {
            Schema::drop('standar_limas');
        }
    }
}
