<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;

class standard_five extends Authenticatable
{
    protected $fillable = [
        'kompetensi_utama,', 'kompetensi_pendukung', 'kompetensi_lainnya', 'jml_sks_prodi', 'matkul', 'matkul_pilihan', 'matkul_praktek', 'mekanisme_kurikulum', 'hasil_kurikulum', 'mekanisme_matkul', 'bimbingan_akademik', 'avg_skripsi', 'avg_jml_pertemuan', 'sosialisasi_pelaksanaan', 'avg_skirpsi_bulan', 'kurikulum_skripsi', 'suasana_akademik', 'ketersediaan_akademik', 'akademik_kondusif', 'interaksi_akademik', 'pengembangan_perilaku', 'sum',
    ];

    protected $guarded = [
		'prodi_id',
    ];

    protected $hidden = [
		'id',
    ];

    public function prodi()
	{
		return $this->belongsTo('App\Models\prody_identities');
	}
}
