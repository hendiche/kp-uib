<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProdyIdentities extends Migration
{
/**
* Run the migrations.
*
* @return void
*/
public function up()
{
  Schema::create('prodi_identitas', function (Blueprint $table) {
    $table->increments('id');
    $table->string('program_studi');
    $table->string('jurusan');
    $table->string('fakultas');
    $table->string('perguruan_tinggi');
    $table->string('no_sk_prodi');
    $table->date('tgl_sk_prodi');
    $table->string('pejabat_sk_prodi');
    $table->date('penyelenggara_prodi');
    $table->string('no_sk_izin');
    $table->date('tgl_sk_izin');
    $table->string('peringkat_akreditas');
    $table->string('no_sk_banpt');
    $table->string('alamat_prodi');
    $table->string('no_telp_prodi');
    $table->string('no_faksimili_prodi');
    $table->string('email_prodi');
    $table->integer('jumlah_akhir')->nullable();
    $table->integer('nilai_3b')->nullable();
    $table->string('akreditasi')->nullable();
    $table->timestamps();
  });
}

/**
* Reverse the migrations.
*
* @return void
*/
public function down()
{
    if (Schema::hasTable('prodi_identitas'))
        {
            Schema::drop('prodi_identitas');
        }
    }
}