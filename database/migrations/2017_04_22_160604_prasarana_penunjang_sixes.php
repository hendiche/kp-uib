<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PrasaranaPenunjangSixes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prasarana_penunjangs', function(Blueprint $table){
            $table->increments('id');
            $table->string('jenis');
            $table->integer('jumlah');
            $table->integer('luas');
            $table->string('sd');
            $table->string('sw');
            $table->string('terawat');
            $table->string('tdk_terawat');
            $table->string('unit');
            $table->unsignedInteger('standar6_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('prasarana_penunjangs'))
        {
            Schema::drop('prasarana_penunjangs');
        }
    }
}
