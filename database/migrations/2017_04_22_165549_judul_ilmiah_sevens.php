<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class JudulIlmiahSevens extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('judul_ilmiahs', function(Blueprint $table){
            $table->increments('id');
            $table->string('judul');
            $table->string('nama_dosen');
            $table->date('publikasi');
            $table->date('penyajian');
            $table->string('lokal');
            $table->string('nasional');
            $table->string('internasional');
            $table->unsignedInteger('standar7_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('judul_ilmiahs'))
        {
            Schema::drop('judul_ilmiahs');
        }
    }
}
