<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PerbaikanFives extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('perbaikans', function(Blueprint $table){
            $table->increments('id');
            $table->string('tindakan');
            $table->string('hasil');
            $table->unsignedInteger('standar5_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('perbaikans'))
        {
            Schema::drop('perbaikans');
        }
    }
}
