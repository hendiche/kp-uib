<!DOCTYPE html>
<html>
<head>
</head>
<body>
  <div id="navigation">
    <div class="row">
      <div class="list-group">
        <div class="col-md-12">
          <a href="{{ url('/') }}" class="list-group-item">Beranda</a>
        </div>
        <div class="col-md-12">
          <a href="{{ url('/prodi/index') }}" class="list-group-item">Identitas Prodi</a>
        </div>
        <div class="col-md-12">
          <a href="{{ url('/standart1/index') }}" class="list-group-item">Standar 1</a>
        </div>
        <div class="col-md-12">
          <a href="{{ url('/standart2/index') }}" class="list-group-item">Standar 2</a>
        </div>
        <div class="col-md-12">
          <a href="{{ url('/standart3/index') }}" class="list-group-item">Standar 3</a>
        </div>
        <div class="col-md-12">
          <a href="{{ url('/standart4/index') }}" class="list-group-item">Standar 4</a>
        </div>
        <div class="col-md-12">
          <a href="{{ url('/standart5/index') }}" class="list-group-item">Standar 5</a>
        </div>
        <div class="col-md-12">
          <a href="{{ url('/standart6/index') }}" class="list-group-item">Standar 6</a>
        </div>
        <div class="col-md-12">
          <a href="{{ url('/standart7/index') }}" class="list-group-item">Standar 7</a>
        </div>
        <div class="col-md-12">
          <a href="{{ url('/user/index') }}" class="list-group-item">Membuat Pengguna Baru</a>
        </div>
        <div class="col-md-12">
          <a href="{{ route('dosen.index') }}" class="list-group-item">Dosen</a>
        </div>
      </div>
    </div>
  </div>
</body>
</html>