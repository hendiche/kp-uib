<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class HasilKurikulumFives extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hasil_kurikulums', function(Blueprint $table){
            $table->increments('id');
            $table->string('no_mk');
            $table->string('nama_mk');
            $table->string('jenis_mk');
            $table->string('sap');
            $table->string('buku_ajar');
            $table->string('alasan');
            $table->string('usulan');
            $table->date('tahun');
            $table->unsignedInteger('standar5_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('hasil_kurikulums'))
        {
            Schema::drop('hasil_kurikulums');
        }
    }
}
