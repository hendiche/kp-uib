<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;

class mhs_non_reguler_3 extends Authenticatable
{
    protected $fillable = [
		'thn_akademik', 'calon_mhs_ikut', 'calon_mhs_lulus', 'mhs_baru_no_trans', 'mhs_baru_trans', 'total_mhs_no_trans', 'total_mhs_trans'
    ];

    protected $guarded = [
    	'id'
    ];

    protected $hidden = [
		
    ];

    protected $dates = [
        'updated_at', 'created_at'
    ];

    public function standardThree()
	{
		return $this->belongsTo('App\Models\standard_three');
	}
}
