<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class users extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new user();

        $user->nama = "admin";
        $user->email = "admin@admin.com";
        $user->password = hash::make('adminadmin');
        $user->peran = "admin";

        $user-> save();
    }
}
