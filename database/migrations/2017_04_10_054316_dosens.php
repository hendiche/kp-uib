<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Dosens extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dosens', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('no');
          $table->string('nama');
          $table->string('nidn');
          $table->date('tgl_lahir');
          $table->string('jabatan');
          $table->string('gelar');
          $table->string('pendidikan');
          $table->string('bidang_keahlian');
          $table->enum('tipe_dosen', ['tetap', 'tdk_tetap']);
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('dosens'))
        {
          Schema::drop('dosens');
        }
    }
}
