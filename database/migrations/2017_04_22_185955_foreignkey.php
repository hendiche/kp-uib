<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Foreignkey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('standar_dosens', function($table) {
          $table->foreign('prodi_id')->references('id')->on('prodi_identitas');
          $table->foreign('standar4_id')->references('id')->on('standar_empats');
          $table->foreign('dosen_id')->references('id')->on('dosens');
        });

        Schema::table('pengisi_borangs', function($table) {
          $table->foreign('user_id')->references('id')->on('users');
          $table->foreign('prodi_id')->references('id')->on('prodi_identitas');
        });

        Schema::table('standar_satus', function($table) {
          $table->foreign('prodi_id')->references('id')->on('prodi_identitas');
        });

        // =============== standar 2 ===================
        Schema::table('standar_duas', function($table) {
          $table->foreign('prodi_id')->references('id')->on('prodi_identitas');
        });
        Schema::table('umpan_balik_duas', function($table) {
          $table->foreign('standar2_id')->references('id')->on('standar_duas');
        });
        // ================  standar 2 =======================

        // ================ standar 3 ======================
        Schema::table('standar_tigas', function($table) {
          $table->foreign('prodi_id')->references('id')->on('prodi_identitas');
        });
        Schema::table('mhs_reguler_tigas', function($table) {
          $table->foreign('standar3_id')->references('id')->on('standar_tigas');
        });
        Schema::table('mhs_non_reguler_tigas', function($table) {
          $table->foreign('standar3_id')->references('id')->on('standar_tigas');
        });
        Schema::table('prestasi_tigas', function($table) {
          $table->foreign('standar3_id')->references('id')->on('standar_tigas');
        });
        Schema::table('mhs_7thn_tigas', function($table) {
          $table->foreign('standar3_id')->references('id')->on('standar_tigas');
        });
        Schema::table('layanan_mhs_tigas', function($table) {
          $table->foreign('standar3_id')->references('id')->on('standar_tigas');
        });
        Schema::table('hasil_studi_tigas', function($table) {
          $table->foreign('standar3_id')->references('id')->on('standar_tigas');
        });
        // ================= standar 3 =====================

        // ================= standar 4 =====================
        Schema::table('standar_empats', function($table) {
          $table->foreign('prodi_id')->references('id')->on('prodi_identitas');
        });
        Schema::table('aktivitas_dosen_tetap_empats', function($table) {
          $table->foreign('standar4_id')->references('id')->on('standar_empats');
        });
        Schema::table('aktivitas_ngajar_dosen_empats', function($table) {
          $table->foreign('standar4_id')->references('id')->on('standar_empats');
          $table->foreign('aktivitas_dosen_id')->references('id')->on('aktivitas_dosens');
        });
        Schema::table('tenaga_pembicara_empats', function($table) {
          $table->foreign('standar4_id')->references('id')->on('standar_empats');
        });
        Schema::table('kemampuan_dosen_empats', function($table) {
          $table->foreign('standar4_id')->references('id')->on('standar_empats');
        });
        Schema::table('kegiatan_dosen_empats', function($table) {
          $table->foreign('standar4_id')->references('id')->on('standar_empats');
        });
        Schema::table('pencapaian_dosen_empats', function($table) {
          $table->foreign('standar4_id')->references('id')->on('standar_empats');
        });
        Schema::table('keikutsertaan_empats', function($table) {
          $table->foreign('standar4_id')->references('id')->on('standar_empats');
        });
        Schema::table('tenaga_kependidikan_empats', function($table) {
          $table->foreign('standar4_id')->references('id')->on('standar_empats');
        });
        // ================= standar 4 ======================

        // ================= standar 5 ======================
        Schema::table('standar_limas', function($table) {
          $table->foreign('prodi_id')->references('id')->on('prodi_identitas');
        });
        Schema::table('jml_sks_prodis', function($table) {
            $table->foreign('standar5_id')->references('id')->on('standar_limas');
        });
        Schema::table('matkuls', function($table) {
            $table->foreign('standar5_id')->references('id')->on('standar_limas');
        });
        Schema::table('matkul_pilihans', function($table) {
            $table->foreign('standar5_id')->references('id')->on('standar_limas');
        });
        Schema::table('prakteks', function($table) {
            $table->foreign('standar5_id')->references('id')->on('standar_limas');
        });
        Schema::table('hasil_kurikulums', function($table) {
            $table->foreign('standar5_id')->references('id')->on('standar_limas');
        });
        Schema::table('bimbingan_akademiks', function($table) {
            $table->foreign('standar5_id')->references('id')->on('standar_limas');
        });
        Schema::table('proses_akademiks', function($table) {
            $table->foreign('standar5_id')->references('id')->on('standar_limas');
        });
        Schema::table('bimbingan_skripsis', function($table) {
            $table->foreign('standar5_id')->references('id')->on('standar_limas');
        });
        Schema::table('perbaikans', function($table) {
            $table->foreign('standar5_id')->references('id')->on('standar_limas');
        });
        // ================= standar 5 ======================

        // ================= standar 6 ======================
        Schema::table('standar_enams', function($table) {
          $table->foreign('prodi_id')->references('id')->on('prodi_identitas');
        });
        Schema::table('alokasi_danas', function($table) {
          $table->foreign('standar6_id')->references('id')->on('standar_enams');
        });
        Schema::table('penggunaan_danas', function($table) {
          $table->foreign('standar6_id')->references('id')->on('standar_enams');
        });
        Schema::table('dana_penelitians', function($table) {
          $table->foreign('standar6_id')->references('id')->on('standar_enams');
        });
        Schema::table('dana_kegiatans', function($table) {
          $table->foreign('standar6_id')->references('id')->on('standar_enams');
        });
        Schema::table('ruang_dosens', function($table) {
          $table->foreign('standar6_id')->references('id')->on('standar_enams');
        });
        Schema::table('prasaranas', function($table) {
          $table->foreign('standar6_id')->references('id')->on('standar_enams');
        });
        Schema::table('prasarana_penunjangs', function($table) {
          $table->foreign('standar6_id')->references('id')->on('standar_enams');
        });
        Schema::table('rekapitulasi_pustakas', function($table) {
          $table->foreign('standar6_id')->references('id')->on('standar_enams');
        });
        Schema::table('jurnal_tersedias', function($table) {
          $table->foreign('standar6_id')->references('id')->on('standar_enams');
        });
        Schema::table('peralatan_utamas', function($table) {
          $table->foreign('standar6_id')->references('id')->on('standar_enams');
        });
        Schema::table('aksesibilitas', function($table) {
          $table->foreign('standar6_id')->references('id')->on('standar_enams');
        });
        // ================= standar 6 ======================

        // ================= standar 7 ======================
        Schema::table('standar_tujuhs', function($table) {
          $table->foreign('prodi_id')->references('id')->on('prodi_identitas');
        });
        Schema::table('sumber_biayas', function($table) {
            $table->foreign('standar7_id')->references('id')->on('standar_tujuhs');
        });
        Schema::table('judul_ilmiahs', function($table) {
          $table->foreign('standar7_id')->references('id')->on('standar_tujuhs');
        });
        Schema::table('karya_haks', function($table) {
          $table->foreign('standar7_id')->references('id')->on('standar_tujuhs');
        });
        Schema::table('sumber_danas', function($table) {
          $table->foreign('standar7_id')->references('id')->on('standar_tujuhs');
        });
        Schema::table('instansis', function($table) {
          $table->foreign('standar7_id')->references('id')->on('standar_tujuhs');
        });
        // ================= standar 7 ======================
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('standar_dosens', function($table) {
          $table->dropForeign(['prodi_id']);
          $table->dropForeign(['standar4_id']);
          $table->dropForeign(['dosen_id']);
        });

        Schema::table('pengisi_borangs', function($table) {
          $table->dropForeign(['user_id']);
          $table->dropForeign(['prodi_id']);
        });

        Schema::table('standar_satus', function($table) {
          $table->dropForeign(['prodi_id']);
        });

        // =============== standar 2 ===============================
        Schema::table('standar_duas', function($table) {
          $table->dropForeign(['prodi_id']);
        });
        Schema::table('umpan_balik_duas', function($table) {
          $table->dropForeign(['standar2_id']);
        });
        // ================ standar 2 =================================

        // ================ standar 3 =============================
        Schema::table('standar_tigas', function($table) {
          $table->dropForeign(['prodi_id']);
        });
        Schema::table('mhs_reguler_tigas', function($table) {
          $table->dropForeign(['standar3_id']);
        });
        Schema::table('mhs_non_reguler_tigas', function($table) {
          $table->dropForeign(['standar3_id']);
        });
        Schema::table('prestasi_tigas', function($table) {
          $table->dropForeign(['standar3_id']);
        });
        Schema::table('mhs_7thn_tigas', function($table) {
          $table->dropForeign(['standar3_id']);
        });
        Schema::table('layanan_mhs_tigas', function($table) {
          $table->dropForeign(['standar3_id']);
        });
        Schema::table('hasil_studi_tigas', function($table) {
          $table->dropForeign(['standar3_id']);
        });
        // ================= standar 3 ============================

        // ================= standar 4 ============================
        Schema::table('standar_empats', function($table) {
          $table->dropForeign(['prodi_id']);
        });
        Schema::table('aktivitas_dosen_tetap_empats', function($table) {
          $table->dropForeign(['standar4_id']);
        });
        Schema::table('aktivitas_ngajar_dosen_empats', function($table) {
          $table->dropForeign(['standar4_id']);
          $table->dropForeign(['aktivitas_dosen_id']);
        });
        Schema::table('tenaga_pembicara_empats', function($table) {
          $table->dropForeign(['standar4_id']);
        });
        Schema::table('kemampuan_dosen_empats', function($table) {
          $table->dropForeign(['standar4_id']);
        });
        Schema::table('kegiatan_dosen_empats', function($table) {
          $table->dropForeign(['standar4_id']);
        });
        Schema::table('pencapaian_dosen_empats', function($table) {
          $table->dropForeign(['standar4_id']);
        });
        Schema::table('keikutsertaan_empats', function($table) {
          $table->dropForeign(['standar4_id']);
        });
        // ================= standar 4 ============================
        // ================= standar 5 ============================
        Schema::table('standar_limas', function($table) {
          $table->dropForeign(['prodi_id']);
        });
        Schema::table('jml_sks_prodis', function($table) {
            $table->dropForeign(['standar5_id']);
        });
        Schema::table('matkuls', function($table) {
            $table->dropForeign(['standar5_id']);
        });
        Schema::table('matkul_pilihans', function($table) {
            $table->dropForeign(['standar5_id']);
        });
        Schema::table('prakteks', function($table) {
            $table->dropForeign(['standar5_id']);
        });
        Schema::table('hasil_kurikulums', function($table) {
            $table->dropForeign(['standar5_id']);
        });
        Schema::table('bimbingan_akademiks', function($table) {
            $table->dropForeign(['standar5_id']);
        });
        Schema::table('proses_akademiks', function($table) {
            $table->dropForeign(['standar5_id']);
        });
        Schema::table('bimbingan_skripsis', function($table) {
            $table->dropForeign(['standar5_id']);
        });
        Schema::table('perbaikans', function($table) {
            $table->dropForeign(['standar5_id']);
        });
        // ================= standar 5 ============================

        // ================= standar 6 ============================
        Schema::table('standar_enams', function($table) {
          $table->dropForeign(['prodi_id']);
        });
        Schema::table('alokasi_danas', function($table) {
            $table->dropForeign(['standar6_id']);
        });
        Schema::table('penggunaan_danas', function($table) {
            $table->dropForeign(['standar6_id']);
        });
        Schema::table('dana_penelitians', function($table) {
            $table->dropForeign(['standar6_id']);
        });
        Schema::table('dana_kegiatans', function($table) {
            $table->dropForeign(['standar6_id']);
        });
        Schema::table('ruang_dosens', function($table) {
            $table->dropForeign(['standar6_id']);
        });
        Schema::table('prasaranas', function($table) {
            $table->dropForeign(['standar6_id']);
        });
        Schema::table('prasarana_penunjangs', function($table) {
            $table->dropForeign(['standar6_id']);
        });
        Schema::table('rekapitulasi_pustakas', function($table) {
            $table->dropForeign(['standar6_id']);
        });
        Schema::table('jurnal_tersedias', function($table) {
            $table->dropForeign(['standar6_id']);
        });
        Schema::table('peralatan_utamas', function($table) {
            $table->dropForeign(['standar6_id']);
        });
        Schema::table('aksesibilitas', function($table) {
            $table->dropForeign(['standar6_id']);
        });
        // ================= standar 6 ============================

        // ================= standar 7 ============================
        Schema::table('standar_tujuhs', function($table) {
          $table->dropForeign(['prodi_id']);
        });
        Schema::table('sumber_biayas', function($table) {
          $table->dropForeign(['standar7_id']);
        });
        Schema::table('judul_ilmiahs', function($table) {
          $table->dropForeign(['standar7_id']);
        });
        Schema::table('karya_haks', function($table) {
          $table->dropForeign(['standar7_id']);
        });
        Schema::table('sumber_danas', function($table) {
          $table->dropForeign(['standar7_id']);
        });
        Schema::table('instansis', function($table) {
          $table->dropForeign(['standar7_id']);
        });
        // ================= standar 7 ============================
    }
}
