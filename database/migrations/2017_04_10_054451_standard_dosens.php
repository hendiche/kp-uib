<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StandardDosens extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('standar_dosens', function(Blueprint $table){
            $table->increments('id');
            $table->unsignedInteger('prodi_id')->nullable();
            $table->unsignedInteger('standar4_id')->nullable();
            $table->unsignedInteger('dosen_id');
            $table->enum('tipe', ['prodi', 'sesuai', 'tdk_sesuai'])->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('standar_dosens'))
        {
            Schema::drop('standar_dosens');
        }
    }
}
