<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class HasilStudiThrees extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hasil_studi_tigas', function(Blueprint $table) {
          $table->increments('id');
          $table->string('jenis_kemampuan');
          $table->string('sgt_baik');
          $table->string('baik');
          $table->string('cukup');
          $table->string('kurang');
          $table->string('rencana_tidak_lanjut');
          $table->unsignedInteger('standar3_id');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      if (Schema::hasTable('hasil_studi_tigas'))
      {
        Schema::drop('hasil_studi_tigas');
      }
    }
}
