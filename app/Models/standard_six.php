<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;

class standard_six extends Authenticatable
{
    protected $fillable = [
        'pengelolaan_data,', 'alokasi_dana', 'penggunaan_dana', 'dana_dosen', 'ruang_dosen', 'rekapitulasi_pustaka', 'jurnal_tersedia', 'sumber_pustaka', 'peralatan_utama', 'fasilitas_prodi', 'aksesibilitas', 'sum',
    ];

    protected $guarded = [
    	'prodi_id'
    ];

    protected $hidden = [
        'id',
    ];

    public function prodi()
  	{
    	return $this->belongsTo('App\Models\prody_identities');
  	}
}
