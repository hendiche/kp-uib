<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PenggunaanDanaSixes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penggunaan_danas', function(Blueprint $table){
            $table->increments('id');
            $table->string('jenis');
            $table->integer('ts-2');
            $table->integer('ts-1');
            $table->integer('ts');
            $table->unsignedInteger('standar6_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('penggunaan_danas'))
        {
            Schema::drop('penggunaan_danas');
        }
    }
}
