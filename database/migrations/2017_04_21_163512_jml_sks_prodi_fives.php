<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class JmlSksProdiFives extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jml_sks_prodis', function(Blueprint $table){
            $table->increments('id');
            $table->string('jenis');
            $table->integer('sks');
            $table->string('keterangan');
            $table->unsignedInteger('standar5_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('jml_sks_prodis'))
        {
          Schema::drop('jml_sks_prodis');
        }
    }
}
