@extends('layouts.app')

@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-md-2 col-sm-2 noPadding">
      @include('layouts.navigation')
    </div>
    <div class="col-md-10 col-sm-10 noPadding">
      <div class="panel panel-default">
        <div class="panel-heading">
          <p>Hello, {{Auth::user()->nama}} </p>
        </div>
        <div class="panel-body">
          <table class="table" id="myTable">
            <thead>
              <tr>
                <td>Name</td>
                <td>3A</td>
                <td>3B</td>
                <td>Akreditasi</td>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>Sistem Informasi</td>
                <td>359</td>
                <td>360</td>
                <td>B</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
  $(document).ready(function(){
    $('#myTable').DataTable();
  });
</script>
@endsection
