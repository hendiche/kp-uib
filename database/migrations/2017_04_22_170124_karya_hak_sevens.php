<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class KaryaHakSevens extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('karya_haks', function(Blueprint $table){
            $table->increments('id');
            $table->string('karya');
            $table->unsignedInteger('standar7_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('karya_haks'))
        {
            Schema::drop('karya_haks');
        }
    }
}
