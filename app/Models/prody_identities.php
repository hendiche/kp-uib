<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;

class prody_identities extends Authenticatable
{
    use Notifiable;

    protected $table = 'prodi_identitas';

    protected $fillable = [
        'program_studi', 'jurusan', 'fakultas', 'perguruan_tinggi', 'no_sk_prodi', 'tgl_sk_prodi',
        'pejabat_sk_prodi', 'penyelenggara_prodi', 'no_sk_izin', 'tgl_sk_izin', 'peringkat_akreditas',
        'no_sk_banpt', 'alamat_prodi', 'no_telp_prodi', 'no_faksimili_prodi', 'email_prodi',
    ];

    protected $guarded = [

    ];

    protected $hidden = [
        'id',
    ];

    protected $table = 'prodi_identitas';

    public function Users()
    {
      return $this->belongsToMany('App\Models\User')->withPivot('tgl_isi');
    }

    public function dosen()
    {
      return $this->belongsToMany('App\Models\dosen')->withPivot('type');
    }

    public function standardOne()
    {
      return $this->hasOne('App\Models\standard_one');
    }

    public function standardTwo()
    {
      return $this->hasOne('App\Models\standard_two');
    }

    public function standardThree()
    {
      return $this->hasOne('App\Models\standard_three');
    }

    public function standardFour()
    {
      return $this->hasOne('App\Models\standard_four');
    }

    public function standardFive()
    {
      return $this->hasOne('App\Models\standard_five');
    }

    public function standardSix()
    {
      return $this->hasOne('App\Models\standard_six');
    }

    public function standardSeven()
    {
      return $this->hasOne('App\Models\standard_seven');
    }
}
