<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BimbinganSkripsiFives extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bimbingan_skripsis', function(Blueprint $table){
            $table->increments('id');
            $table->string('nama');
            $table->integer('jml_mhs');
            $table->unsignedInteger('standar5_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('bimbingan_skripsis'))
        {
            Schema::drop('bimbingan_skripsis');
        }
    }
}
