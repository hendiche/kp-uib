<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;

class standard_four extends Authenticatable
{
    protected $fillable = [
        'seleksi_pengembangan,', 'monitor_evaluasi', 'prodi_kualifikasi', 'sum',
    ];

    protected $guarded = [
    	'prodi_id'
    ];

    protected $hidden = [
        'id',
    ];

    public function prodi()
    {
    	return $this->belongsTo('App\Models\prody_identities');
  	}

    public function dosen()
    {
      return $this->belongstoMany('App\Models\dosen')->withPivot('type');
    }
}
