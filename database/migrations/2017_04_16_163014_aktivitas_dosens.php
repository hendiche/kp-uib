<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AktivitasDosens extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aktivitas_dosens', function (Blueprint $table) {
          $table->increments('id');
          $table->string('nama');
          $table->string('bidang');
          $table->string('kode_matkul');
          $table->string('nama_matkul');
          $table->integer('jumlah_kelas');
          $table->integer('jumlah_direncanakan');
          $table->integer('jumlah_terlaksana');
          $table->enum('tipe_dosen', ['tetap', 'tdk_tetap']);
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('aktivitas_dosens'))
        {
          Schema::drop('aktivitas_dosens');
        }
    }
}
