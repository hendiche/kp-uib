<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TenagaKependidikanFours extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tenaga_kependidikan_empats', function(Blueprint $table) {
          $table->increments('id');
          $table->string('jenis');
          $table->integer('pendidikan_s3');
          $table->integer('pendidikan_s2');
          $table->integer('pendidikan_s1');
          $table->integer('pendidikan_d4');
          $table->integer('pendidikan_d3');
          $table->integer('pendidikan_d2');
          $table->integer('pendidikan_d1');
          $table->integer('pendidikan_sma');
          $table->integer('unit');
          $table->unsignedInteger('standar4_id');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('tenaga_kependidikan_empats'))
        {
          Schema::drop('tenaga_kependidikan_empats');
        }
    }
}
