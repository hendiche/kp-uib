<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StandardSevens extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('standar_tujuhs', function (Blueprint $table) {
            $table->increments('id');

            //$table->unsignedInteger('sumber_biaya')->nullable();
            $table->string('mhs_skripsi')->nullable();
            $table->string('jml_mhs_skripsi')->nullable();

            //$table->unsignedInteger('judul_ilmiah')->nullable();
            //$table->unsignedInteger('karya_hak')->nullable();
            //$table->unsignedInteger('sumber_dana')->nullable();

            $table->string('tingkat_partisipasi')->nullable();

            //$table->unsignedInteger('nasional')->nullable();
            //$table->unsignedInteger('internasional')->nullable();

            $table->integer('jumlah')->nullable();
            $table->unsignedInteger('prodi_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('standar_tujuhs'))
        {
            Schema::drop('standar_tujuhs');
        }
    }
}
