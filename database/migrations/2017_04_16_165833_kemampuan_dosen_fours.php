<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class KemampuanDosenFours extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kemampuan_dosen_empats', function (Blueprint $table) {
          $table->increments('id');
          $table->string('nama');
          $table->string('pendidikan');
          $table->string('bidang_studi');
          $table->string('perguruan_tinggi');
          $table->string('negara');
          $table->date('tahun_mulai');
          $table->unsignedInteger('standar4_id');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('kemampuan_dosen_empats'))
        {
          Schema::drop('kemampuan_dosen_empats');
        }
    }
}
