@extends('layouts.app')

@section('content')
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>{{ config('app.name', 'Laravel') }}</title>

	<!-- Styles -->
	<link href="/css/app.css" rel="stylesheet">

	<!-- Scripts -->
</head>

<body>
	<div class="container">
		<div class="panel panel-default">
			<div class="panel panel-heading">
			<a href="#">
				<span>Standart 3</span>
			</a>
			</div>
			<div class="panel-body">
				{!! Form::open(['url' => '#', 'method' => 'post']) !!}
				<table class="table">
				<tr>
					<td>
					<div>
            {{ Form::label('', 'Profil Mahasiswa dan Lulusan') }} <br/>
            {{ Form::label('', 'Tuliskan data seluruh mahasiswa reguler(1) dan lulusannya dalam lima tahun terakhir dengan mengikuti format tabel berikut:') }}</div>
            <Button type="button" class="btn btn-info">Button</Button>
            <p class="catatan">Catatan: <br/>
            TS:Tahun akademik penuh terakhir saat pengisian borang<br/>
            Min: IPK Minimum; Rat:IPK Rata-rata; Mak:IPK Maksimum<br/>
            Catatan: <br/>
            (1)  Mahasiswa program reguler adalah mahasiswa yang mengikuti program pendidikan secara penuh waktu (baik kelas pagi, siang, sore, malam, dan di seluruh kampus). <br/>
            (2)  Mahasiswa program non-reguler adalah mahasiswa yang mengikuti program pendidikan secara paruh waktu.<br/>
            (3)  Mahasiswa transfer adalah mahasiswa yang masuk ke program studi dengan mentransfer mata kuliah yang telah diperolehnya dari PS lain, baik dari dalam PT maupun luar PT. <br/>
            </p>
					</td>
				</tr>
				<tr>
					<td>
					<div>
            {{ Form::label('', 'Tuliskan data mahasiswa non-reguler(2) dalam limta tahun terakhir dengan mengikuti format tabel berikut:') }}</div>
					 <Button type="button" class="btn btn-info">Button</Button>
          </td>
				</tr>
				<tr>
					<td>
					<div>
            {{ Form::label('', 'Sebutkan pencapaian prestasi/reputasi mahasiswa dalam tiga tahun terakhir di bidang akademik dan non-akademik (misalnya prestasi dalam penelitian dan lomba karya ilmiah, olahraga, dan seni).') }}</div>
            <button type="button" class="btn btn-info">Button</button>
            </p>
          </td>
				</tr>
				<tr>
					<td>
					<div>
            {{ Form::label('', 'Tuliskan data jumlah mahasiswa reguler tujuh tahun terakhir dengan mengikuti format tabel berikut:') }}</div>
            <button type="button" class="btn btn-info">Button</button>
            <p class="catatan">
              Catatan : huruf-huruf a, b, c, d, e dan f harus tetap tercantum pada tabel di atas.
					</td>
				</tr>
				<tr>
					<td>
					<div>
            {{ Form::label('', 'Layanan kepada Mahasiswa') }} <br/>
            {{ Form::label('', 'Lengkapilah tabel berikut untuk setiap jenis pelayanan kepada mahasiswa PS:') }} <br/>
            <button type="button" class="btn btn-info">Button</button>
          </div>
					</td>
				</tr>
				<tr>
					<td>
					<div>
            {{ Form::label('', 'Evaluasi Lulusan') }} <br/>
            {{ Form::label('', 'Evaluasi Kinerja lulusan oleh Pihak Pengguna Lulusan') }} <br/>
            {{ Form::label('standart3', 'Adakah studi pelacakan (tracer study) untuk mendapat hasil evaluasi kinerja lulusan dengan pihak pengguna?') }}
          </div>
					<div> {{ Form::radio('choose', 'tidak') }} Tidak Ada</div>
					<div> {{ Form::radio('choose', 'ya') }} Ya</div>
					<div> {{ Form::label('', 'Uraikan metode, proses dan mekanisme kegiatan studi pelacakan tersebut. Jelaskan pula bentuk tindak lanjut dari hasil kegiatan ini.') }}
					<div> {{ Form::textarea('evaluasi', null, ['required' => '', 'class' => 'textarea' ]) }}</div>
          <div>
            {{ Form::label('', 'Hasil studi pelacakan dirangkum dalam tabel berikut:') }}<br/>
            {{ Form::label('', 'Nyatakan angka persentasenya pada kolom yang sesuai.') }}<br/>
            <p class="catatan">
            Catatan: Sediakan dokumen pendukung pada saat asesmen lapangan <br/>
            (*) persentase tanggapan pihak pengguna = [(jumlah tanggapan pada peringkat) : (jumlah tanggapan yang ada)] x 100
            </p>
          </div>
					</td>
				</tr>
        <tr>
          <td>
            <div>
              <p class="bold">
                Rata-rata waktu tunggu lulusan untuk memperoleh pekerjaan yang pertama = {{ Form::text('jumlah_bulan', null, ['required' => '']) }} bulan (Jelaskan bagaimana data ini diperoleh)
              </p>
            </div>
          </td>
        </tr>
        <tr>
          <td>
            <div>
              <p class="bold">
                Persentase lulusan yang bekerja pada bidang yang sesuai dengan keahliannya = {{ Form::text('persentase', null, ['required' => '']) }} % (Jelaskan bagaimana data ini diperoleh)
              </p>
            </div>
          </td>
        </tr>
				<tr>
					<td>
					<div>
            {{ Form::label('', 'Himpunan Alumni') }} <br/>
            {{ Form::label('', 'Jelaskan apakah lulusan program studi memiliki himpunan alumni. Jika memiliki,
            jelaskan aktivitas dan hasil kegiatan dari himpunan alumni untuk kemajuan progrma studi dalam kegiatan
            akademik dan non akademik, meliputi sumbangan dana, sumbangan fasilitas, keterlibatan dalam kegiatan,
            pengembangan jejaring, dan penyediaan fasilitas.') }}
          </div>
					<div> {{ Form::textarea('himpunan', null, ['required' => '', 'class' => 'textarea' ]) }}</div>
					</td>
				</tr>

				<tr>
					<td colspan="2">
					{{ Form::submit('confirm', ['class' => 'btn btn-info']) }}
					<a href="{{ url('home') }}" class="btn btn-warning">Back</a>
					</td>
				</tr>
				</table>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</body>

</html>
@endsection