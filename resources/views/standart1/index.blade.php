@extends('layouts.app')

@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-md-2 col-sm-2 noPadding" >
      @include('layouts.navigation')
    </div>
    <div class="col-md-10 col-sm-10 noPadding">
      <div class="panel-heading">
      <a href="#">
        <span>Standart 1</span>
      </a>
      </div>
      <div class="panel-body">
        {!! Form::open(['url' => 'standart1/insert', 'method' => 'post']) !!}
        <table class="table">
          <tr>
            <td>
              <div> {{ Form::label('', 'Pilih Prodi yang akan di lanjut untuk pengisian isi') }} </div>
              <div>
                <select name="prodi" required>
                  <option value="">Pilih Prodi</option>
                  @foreach($prodi as $list)
                    <option value="{{ $list->id }}">{{ $list->program_studi }} - {{ $list->jurusan }}</option>
                  @endforeach
                </select>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div> {{ Form::label('', 'Jelaskan mekanisme penyusunan visi, misi, tujuan dan sasaran program studi, serta pihak-pihak yang dilibatkan') }}</div>
              <div>{{ Form::textarea('mekanisme', null, ['required' => '', 'class' => 'textarea' ]) }}</div>
            </td>
          </tr>
          <tr>
            <td>
              <div> {{ Form::label('', 'Visi') }}</div>
              <div>{{ Form::textarea('visi', null, ['required' => '', 'class' => 'textarea' ]) }}</div>
            </td>
          </tr>
          <tr>
            <td>
              <div> {{ Form::label('', 'Misi') }}</div>
              <div>{{ Form::textarea('misi', null, ['required' => '', 'class' => 'textarea' ]) }}</div>
            </td>
          </tr>
          <tr>
            <td>
              <div> {{ Form::label('', 'Tujuan') }}</div>
              <div>{{ Form::textarea('tujuan', null, ['required' => '', 'class' => 'textarea' ]) }}</div>
            </td>
          </tr>
          <tr>
            <td>
              <div> {{ Form::label('', 'Sasaran dan Strategi Pencapaiannya') }}</div>
              <div>{{ Form::textarea('sasaran', null, ['required' => '', 'class' => 'textarea' ]) }}</div>
            </td>
          </tr>
          <tr>
            <td>
              <div> {{ Form::label('', 'Sosialisasi') }} <br/>
                {{ Form::label('', 'Uraikan upaya penyebaran/sosialisasi viis, misi dan tujuan program studi serta pemahaman sivitas akademika (dosen dan mahasiswa) dan tenaga kependidikan.') }}
              </div>
              <div>{{ Form::textarea('sosialisasi', null, ['required' => '', 'class' => 'textarea' ]) }}</div>
            </td>
          </tr>
          <tr>
            <td colspan="2">
            {{ Form::submit('confirm', ['class' => 'btn btn-info']) }}
            </td>
          </tr>
        </table>
        {!! Form::close() !!}
      </div>
    </div>
  </div>
</div>
@endsection