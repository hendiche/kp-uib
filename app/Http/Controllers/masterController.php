<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\View\View;
use Yajra\Datatables\Datatables;

class masterController extends Controller
{
    protected $attributes = [
      'title' => 'TITLE',
      'method' => 'POST',
      'route' => ''
    ];

    protected $index_view;

    public function index()
    {
      return $this->render(view($this->index_view));
    }

    public function render(View $view, $model = null)
    {
      return $view->with($this->attributes)->with(compact('model'));
    }

    public function dataTable($model = null)
    {
      $builder = $this->prepareDataTable($model);
      return $this->dataTableMaker($builder);
    }

    public function prepareDataTable($model)
    {
      return Datatables::of($model::query());
    }

    public function sendErrorResponse($errors)
    {
        if (property_exists($this, 'redirect_when_error')) {
            return redirect()->route($this->redirect_when_error);
        }
        return redirect()->back()->withInput()->withErrors($errors);
    }

    public function sendSuccessResponse()
    {
        if (property_exists($this, 'redirect_when_success')) {
            return redirect()->route($this->redirect_when_success);
        }
        return back();
    }
}
