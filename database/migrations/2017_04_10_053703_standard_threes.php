<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StandardThrees extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('standar_tigas', function (Blueprint $table) {
            $table->increments('id');
            // $table->unsignedInteger('mhs_prestasi')->nullable();
            // $table->unsignedInteger('jml_mhs')->nullable();
            // $table->unsignedInteger('layanan_mhs')->nullable();
            $table->string('evaluasi_lulusan')->nullable();
            $table->integer('kerja_pertama')->nullable();
            $table->string('kerja_sesuai_bidang')->nullable();
            // $table->unsignedInteger('hasil_studi_evaluasi')->nullable();
            $table->string('himpunan_alumni')->nullable();
            $table->integer('jumlah')->nullable();
            $table->unsignedInteger('prodi_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('standar_tigas'))
        {
            Schema::drop('standar_tigas');
        }
    }
}
